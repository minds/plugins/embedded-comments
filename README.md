# Minds Embedded Comments Plugins

## Dev setup

Change `proxy` in package.json to reflect your local tenant hostname.

```
npm i
npm run start
```

You can view the app at http://mark.local:3000/plugins/embedded-comments/?pageUrl=https://developers.minds.com/docs/plugins/embedded-comments&ownerGuid=1540482017907445761

## Installing the plugins

See instructions at https://developers.minds.com/docs/plugins/embedded-comments
