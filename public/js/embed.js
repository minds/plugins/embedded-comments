/**
 * The Minds Embedded Comments SDK
 * @license MIT
 * @author Mark Harding
 */
let timer;
let currentScript;

(function MindsEmbeddedCommentsSdk() {
  currentScript = document.currentScript;

  if (!getCommentElement()) {
    // We will try and pick up the comment script during a future dom mutation
    return;
  }

  setupIframe();
})();

function setupIframe() {
  if (isIframeSetup()) return;

  const commentElement = getCommentElement();

  // mark as already setup
  commentElement.setAttribute("data-ready", true);

  // get information from the tags
  const href = commentElement.getAttribute("data-href");
  const ownerGuid = commentElement.getAttribute("data-owner-guid");
  const theme = commentElement.getAttribute("data-theme") ?? 'light';
  const mindsBranded = commentElement.getAttribute("data-minds-branded") !== 'false';
  const accentColor = commentElement.getAttribute("data-accent-color");
  // build the embed app link
  const embeddedCommentsAppUrl =
    commentElement.getAttribute("data-app-url") ||
    currentScript.src.replace("/js/embed.js", "");
  let iframeSrc =
    embeddedCommentsAppUrl + "/?pageUrl=" + href + "&ownerGuid=" + ownerGuid;

  if (theme && ['light', 'dark'].includes(theme)) {
    iframeSrc += `&theme=${theme}`;
  }

  if (accentColor) {
    iframeSrc += `&accentColor=${encodeURIComponent(accentColor)}`;
  }

  if (mindsBranded !== null) {
    iframeSrc += `&mindsBranded=${mindsBranded}`;
  }

  // create an iframe
  const iframe = document.createElement("iframe");
  iframe.setAttribute("name", "minds-comments");
  iframe.setAttribute("src", iframeSrc);
  iframe.setAttribute(
    "style",
    "border: none; visibility: visible; width: 100%; height: 200px;",
  );
  iframe.setAttribute("allow", "encrypted-media");

  commentElement.appendChild(iframe);
}

function getCommentElement() {
  const elements = document.querySelectorAll(".minds-comments");

  if (!elements.length) {
    return null;
  }

  return elements[0];
}

function isIframeSetup() {
  const commentElement = getCommentElement();

  if (!commentElement) return false;

  return commentElement.getAttribute("data-ready") === 'true';
}

const observer = new MutationObserver(function () {
  if (timer) clearTimeout(timer);

  timer = setTimeout(() => {
    if (isIframeSetup()) return;

    if (getCommentElement()) {
      setupIframe();
    }
  }, 300); // Wait 300ms between mutatations
});
observer.observe(document.documentElement || document.body, {
  attributes: true,
  childList: true,
});

/**
 * The react app should call this on observation changes so we can auto adjust the height
 */
window.addEventListener('message', function (event) {
  if (event.data.fn === "AutoMindsEmbeddedCommentsSdkHeight") {
    const iframe = getCommentElement().querySelectorAll('iframe')[0];
    iframe.style.height = event.data.height + 'px';
  }
});
