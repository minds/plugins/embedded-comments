/* eslint-disable */
import * as types from "./graphql";
import { TypedDocumentNode as DocumentNode } from "@graphql-typed-document-node/core";

/**
 * Map of all GraphQL operations in the project.
 *
 * This map has several performance disadvantages:
 * 1. It is not tree-shakeable, so it will include all operations in the project.
 * 2. It is not minifiable, so the string of a GraphQL query will be multiple times inside the bundle.
 * 3. It does not support dead code elimination, so it will add unused operations.
 *
 * Therefore it is highly recommended to use the babel or swc plugin for production.
 */
const documents = {
  "\n  mutation CreateComment(\n    $ownerGuid: String!,\n    $url: String!,\n    $parentPath: String!,\n    $body: String!\n) {\n    createEmbeddedComment(\n        ownerGuid: $ownerGuid\n        url: $url\n        parentPath: $parentPath\n        body: $body\n      ) {\n        id\n        ...CommonCommentEdge\n    }\n  }\n":
    types.CreateCommentDocument,
  "\nfragment CommonCommentEdge on CommentEdge {\n  id\n\n  node {\n    id\n    luid\n    parentPath\n    childPath\n    guid\n    body\n    timeCreated\n    url\n    attachment {\n      type\n      src\n    }\n    owner {\n      guid\n      name\n      iconUrl\n    }\n  }\n  \n  repliesCount\n  hasVotedUp\n  hasVotedDown\n  votesUpCount\n}":
    types.CommonCommentEdgeFragmentDoc,
  "\n  query GetEmbeddedComments(\n    $ownerGuid: String!,\n    $url: String!,\n    $parentPath: String,\n    $limit: Int\n  ) {\n    embeddedComments(\n        ownerGuid: $ownerGuid,\n        url: $url,\n        parentPath: $parentPath\n        first: $limit\n      ) {\n        totalCount\n        activityUrl\n        edges {\n          id\n          ...CommonCommentEdge\n        }\n        pageInfo {\n          hasPreviousPage\n          hasNextPage\n          startCursor\n          endCursor\n        }\n      }\n  }\n":
    types.GetEmbeddedCommentsDocument,
};

/**
 * The gql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 *
 *
 * @example
 * ```ts
 * const query = gql(`query GetUser($id: ID!) { user(id: $id) { name } }`);
 * ```
 *
 * The query argument is unknown!
 * Please regenerate the types.
 */
export function gql(source: string): unknown;

/**
 * The gql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function gql(
  source: "\n  mutation CreateComment(\n    $ownerGuid: String!,\n    $url: String!,\n    $parentPath: String!,\n    $body: String!\n) {\n    createEmbeddedComment(\n        ownerGuid: $ownerGuid\n        url: $url\n        parentPath: $parentPath\n        body: $body\n      ) {\n        id\n        ...CommonCommentEdge\n    }\n  }\n",
): (typeof documents)["\n  mutation CreateComment(\n    $ownerGuid: String!,\n    $url: String!,\n    $parentPath: String!,\n    $body: String!\n) {\n    createEmbeddedComment(\n        ownerGuid: $ownerGuid\n        url: $url\n        parentPath: $parentPath\n        body: $body\n      ) {\n        id\n        ...CommonCommentEdge\n    }\n  }\n"];
/**
 * The gql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function gql(
  source: "\nfragment CommonCommentEdge on CommentEdge {\n  id\n\n  node {\n    id\n    luid\n    parentPath\n    childPath\n    guid\n    body\n    timeCreated\n    url\n    attachment {\n      type\n      src\n    }\n    owner {\n      guid\n      name\n      iconUrl\n    }\n  }\n  \n  repliesCount\n  hasVotedUp\n  hasVotedDown\n  votesUpCount\n}",
): (typeof documents)["\nfragment CommonCommentEdge on CommentEdge {\n  id\n\n  node {\n    id\n    luid\n    parentPath\n    childPath\n    guid\n    body\n    timeCreated\n    url\n    attachment {\n      type\n      src\n    }\n    owner {\n      guid\n      name\n      iconUrl\n    }\n  }\n  \n  repliesCount\n  hasVotedUp\n  hasVotedDown\n  votesUpCount\n}"];
/**
 * The gql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function gql(
  source: "\n  query GetEmbeddedComments(\n    $ownerGuid: String!,\n    $url: String!,\n    $parentPath: String,\n    $limit: Int\n  ) {\n    embeddedComments(\n        ownerGuid: $ownerGuid,\n        url: $url,\n        parentPath: $parentPath\n        first: $limit\n      ) {\n        totalCount\n        activityUrl\n        edges {\n          id\n          ...CommonCommentEdge\n        }\n        pageInfo {\n          hasPreviousPage\n          hasNextPage\n          startCursor\n          endCursor\n        }\n      }\n  }\n",
): (typeof documents)["\n  query GetEmbeddedComments(\n    $ownerGuid: String!,\n    $url: String!,\n    $parentPath: String,\n    $limit: Int\n  ) {\n    embeddedComments(\n        ownerGuid: $ownerGuid,\n        url: $url,\n        parentPath: $parentPath\n        first: $limit\n      ) {\n        totalCount\n        activityUrl\n        edges {\n          id\n          ...CommonCommentEdge\n        }\n        pageInfo {\n          hasPreviousPage\n          hasNextPage\n          startCursor\n          endCursor\n        }\n      }\n  }\n"];

export function gql(source: string) {
  return (documents as any)[source] ?? {};
}

export type DocumentType<TDocumentNode extends DocumentNode<any, any>> =
  TDocumentNode extends DocumentNode<infer TType, any> ? TType : never;
