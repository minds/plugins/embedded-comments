/* eslint-disable */
import { TypedDocumentNode as DocumentNode } from "@graphql-typed-document-node/core";
export type Maybe<T> = T | null;
export type InputMaybe<T> = Maybe<T>;
export type Exact<T extends { [key: string]: unknown }> = {
  [K in keyof T]: T[K];
};
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & {
  [SubKey in K]?: Maybe<T[SubKey]>;
};
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & {
  [SubKey in K]: Maybe<T[SubKey]>;
};
export type MakeEmpty<
  T extends { [key: string]: unknown },
  K extends keyof T,
> = { [_ in K]?: never };
export type Incremental<T> =
  | T
  | {
      [P in keyof T]?: P extends " $fragmentName" | "__typename" ? T[P] : never;
    };
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: { input: string; output: string };
  String: { input: string; output: string };
  Boolean: { input: boolean; output: boolean };
  Int: { input: number; output: number };
  Float: { input: number; output: number };
  /** The `Void` scalar type represents no value being returned. */
  Void: { input: any; output: any };
};

export type ActivityEdge = EdgeInterface & {
  __typename?: "ActivityEdge";
  cursor: Scalars["String"]["output"];
  explicitVotes: Scalars["Boolean"]["output"];
  id: Scalars["ID"]["output"];
  node: ActivityNode;
  type: Scalars["String"]["output"];
};

export type ActivityNode = NodeInterface & {
  __typename?: "ActivityNode";
  /** Relevant for images/video posts. A blurhash to be used for preloading the image. */
  blurhash?: Maybe<Scalars["String"]["output"]>;
  commentsCount: Scalars["Int"]["output"];
  guid: Scalars["String"]["output"];
  hasVotedDown: Scalars["Boolean"]["output"];
  hasVotedUp: Scalars["Boolean"]["output"];
  id: Scalars["ID"]["output"];
  impressionsCount: Scalars["Int"]["output"];
  /** The activity has comments enabled */
  isCommentingEnabled: Scalars["Boolean"]["output"];
  legacy: Scalars["String"]["output"];
  message: Scalars["String"]["output"];
  nsfw: Array<Scalars["Int"]["output"]>;
  nsfwLock: Array<Scalars["Int"]["output"]>;
  owner: UserNode;
  ownerGuid: Scalars["String"]["output"];
  /** Unix timestamp representation of time created */
  timeCreated: Scalars["Int"]["output"];
  /** ISO 8601 timestamp representation of time created */
  timeCreatedISO8601: Scalars["String"]["output"];
  /** Relevant for images/video posts */
  title?: Maybe<Scalars["String"]["output"]>;
  urn: Scalars["String"]["output"];
  votesDownCount: Scalars["Int"]["output"];
  votesUpCount: Scalars["Int"]["output"];
};

export type AddOn = {
  __typename?: "AddOn";
  description: Scalars["String"]["output"];
  id: Scalars["String"]["output"];
  inBasket: Scalars["Boolean"]["output"];
  monthlyFeeCents?: Maybe<Scalars["Int"]["output"]>;
  name: Scalars["String"]["output"];
  oneTimeFeeCents?: Maybe<Scalars["Int"]["output"]>;
  perks?: Maybe<Array<Scalars["String"]["output"]>>;
  perksTitle: Scalars["String"]["output"];
};

export type AddOnSummary = {
  __typename?: "AddOnSummary";
  id: Scalars["String"]["output"];
  monthlyFeeCents?: Maybe<Scalars["Int"]["output"]>;
  name: Scalars["String"]["output"];
  oneTimeFeeCents?: Maybe<Scalars["Int"]["output"]>;
};

export type AssetConnection = ConnectionInterface & {
  __typename?: "AssetConnection";
  edges: Array<EdgeInterface>;
  pageInfo: PageInfo;
};

export type AttachmentNode = {
  __typename?: "AttachmentNode";
  containerGuid: Scalars["String"]["output"];
  guid: Scalars["String"]["output"];
  height?: Maybe<Scalars["Int"]["output"]>;
  href: Scalars["String"]["output"];
  id: Scalars["ID"]["output"];
  mature?: Maybe<Scalars["Boolean"]["output"]>;
  src: Scalars["String"]["output"];
  type: Scalars["String"]["output"];
  width?: Maybe<Scalars["Int"]["output"]>;
};

export type BoostEdge = EdgeInterface & {
  __typename?: "BoostEdge";
  cursor: Scalars["String"]["output"];
  id: Scalars["ID"]["output"];
  node: BoostNode;
  type: Scalars["String"]["output"];
};

export type BoostNode = NodeInterface & {
  __typename?: "BoostNode";
  activity: ActivityNode;
  goalButtonText?: Maybe<Scalars["Int"]["output"]>;
  goalButtonUrl?: Maybe<Scalars["String"]["output"]>;
  guid: Scalars["String"]["output"];
  id: Scalars["ID"]["output"];
  legacy: Scalars["String"]["output"];
};

export type BoostsConnection = ConnectionInterface & {
  __typename?: "BoostsConnection";
  /** Gets Boost edges in connection. */
  edges: Array<BoostEdge>;
  pageInfo: PageInfo;
};

export type CheckoutPage = {
  __typename?: "CheckoutPage";
  addOns: Array<AddOn>;
  description?: Maybe<Scalars["String"]["output"]>;
  id: CheckoutPageKeyEnum;
  plan: Plan;
  summary: Summary;
  termsMarkdown?: Maybe<Scalars["String"]["output"]>;
  timePeriod: CheckoutTimePeriodEnum;
  title: Scalars["String"]["output"];
  totalAnnualSavingsCents: Scalars["Int"]["output"];
};

export enum CheckoutPageKeyEnum {
  Addons = "ADDONS",
  Confirmation = "CONFIRMATION",
}

export enum CheckoutTimePeriodEnum {
  Monthly = "MONTHLY",
  Yearly = "YEARLY",
}

export type CommentEdge = EdgeInterface & {
  __typename?: "CommentEdge";
  cursor: Scalars["String"]["output"];
  hasVotedDown: Scalars["Boolean"]["output"];
  hasVotedUp: Scalars["Boolean"]["output"];
  id: Scalars["ID"]["output"];
  node: CommentNode;
  repliesCount: Scalars["Int"]["output"];
  type: Scalars["String"]["output"];
  votesUpCount: Scalars["Int"]["output"];
};

export type CommentNode = NodeInterface & {
  __typename?: "CommentNode";
  /** Gets a comments linked AttachmentNode. */
  attachment?: Maybe<AttachmentNode>;
  body: Scalars["String"]["output"];
  childPath: Scalars["String"]["output"];
  guid: Scalars["String"]["output"];
  id: Scalars["ID"]["output"];
  legacy: Scalars["String"]["output"];
  /** Still used for votes, to be removed soon */
  luid: Scalars["String"]["output"];
  nsfw: Array<Scalars["Int"]["output"]>;
  nsfwLock: Array<Scalars["Int"]["output"]>;
  owner: UserNode;
  parentPath: Scalars["String"]["output"];
  /** Unix timestamp representation of time created */
  timeCreated: Scalars["Int"]["output"];
  /** ISO 8601 timestamp representation of time created */
  timeCreatedISO8601: Scalars["String"]["output"];
  url: Scalars["String"]["output"];
  urn: Scalars["String"]["output"];
};

export type Connection = ConnectionInterface & {
  __typename?: "Connection";
  edges: Array<EdgeInterface>;
  pageInfo: PageInfo;
};

export type ConnectionInterface = {
  edges: Array<EdgeInterface>;
  pageInfo: PageInfo;
};

export enum CustomHostnameStatusEnum {
  Active = "ACTIVE",
  ActiveRedeploying = "ACTIVE_REDEPLOYING",
  Blocked = "BLOCKED",
  Deleted = "DELETED",
  Moved = "MOVED",
  Pending = "PENDING",
  PendingBlocked = "PENDING_BLOCKED",
  PendingDeletion = "PENDING_DELETION",
  PendingMigration = "PENDING_MIGRATION",
  PendingProvisioned = "PENDING_PROVISIONED",
  Provisioned = "PROVISIONED",
  TestActive = "TEST_ACTIVE",
  TestActiveApex = "TEST_ACTIVE_APEX",
  TestBlocked = "TEST_BLOCKED",
  TestFailed = "TEST_FAILED",
  TestPending = "TEST_PENDING",
}

export type Dismissal = {
  __typename?: "Dismissal";
  dismissalTimestamp: Scalars["Int"]["output"];
  key: Scalars["String"]["output"];
  userGuid: Scalars["String"]["output"];
};

export enum DnsRecordEnum {
  A = "A",
  Cname = "CNAME",
  Txt = "TXT",
}

export type EdgeImpl = EdgeInterface & {
  __typename?: "EdgeImpl";
  cursor: Scalars["String"]["output"];
  node?: Maybe<NodeInterface>;
};

export type EdgeInterface = {
  cursor: Scalars["String"]["output"];
  node?: Maybe<NodeInterface>;
};

export type EmbeddedCommentsConnection = ConnectionInterface & {
  __typename?: "EmbeddedCommentsConnection";
  /** The url of the activity post */
  activityUrl: Scalars["String"]["output"];
  edges: Array<CommentEdge>;
  pageInfo: PageInfo;
  /** The number of comments found */
  totalCount: Scalars["Int"]["output"];
};

export type EmbeddedCommentsSettings = {
  __typename?: "EmbeddedCommentsSettings";
  autoImportsEnabled: Scalars["Boolean"]["output"];
  domain: Scalars["String"]["output"];
  pathRegex: Scalars["String"]["output"];
  userGuid: Scalars["Int"]["output"];
};

export type FeaturedEntity = FeaturedEntityInterface &
  NodeInterface & {
    __typename?: "FeaturedEntity";
    autoPostSubscription: Scalars["Boolean"]["output"];
    autoSubscribe: Scalars["Boolean"]["output"];
    entityGuid: Scalars["String"]["output"];
    id: Scalars["ID"]["output"];
    /** Gets entity name. */
    name: Scalars["String"]["output"];
    recommended: Scalars["Boolean"]["output"];
    tenantId: Scalars["String"]["output"];
  };

export type FeaturedEntityConnection = ConnectionInterface &
  NodeInterface & {
    __typename?: "FeaturedEntityConnection";
    /** Gets connections edges. */
    edges: Array<FeaturedEntityEdge>;
    /** ID for GraphQL. */
    id: Scalars["ID"]["output"];
    pageInfo: PageInfo;
  };

export type FeaturedEntityEdge = EdgeInterface & {
  __typename?: "FeaturedEntityEdge";
  /** Gets cursor for GraphQL. */
  cursor: Scalars["String"]["output"];
  /** Gets ID for GraphQL. */
  id: Scalars["ID"]["output"];
  /** Gets node - can be either a FeaturedUser or FeaturedGroup. */
  node: FeaturedEntityInterface;
  /** Gets type for GraphQL. */
  type: Scalars["String"]["output"];
};

export type FeaturedEntityInput = {
  autoPostSubscription?: InputMaybe<Scalars["Boolean"]["input"]>;
  autoSubscribe?: InputMaybe<Scalars["Boolean"]["input"]>;
  entityGuid: Scalars["String"]["input"];
  recommended?: InputMaybe<Scalars["Boolean"]["input"]>;
};

export type FeaturedEntityInterface = {
  autoPostSubscription: Scalars["Boolean"]["output"];
  autoSubscribe: Scalars["Boolean"]["output"];
  entityGuid: Scalars["String"]["output"];
  id: Scalars["ID"]["output"];
  /** Gets entity name. */
  name: Scalars["String"]["output"];
  recommended: Scalars["Boolean"]["output"];
  tenantId: Scalars["String"]["output"];
};

export enum FeaturedEntityTypeEnum {
  Group = "GROUP",
  User = "USER",
}

export type FeaturedGroup = FeaturedEntityInterface &
  NodeInterface & {
    __typename?: "FeaturedGroup";
    autoPostSubscription: Scalars["Boolean"]["output"];
    autoSubscribe: Scalars["Boolean"]["output"];
    entityGuid: Scalars["String"]["output"];
    id: Scalars["ID"]["output"];
    /** Gets count of members. */
    membersCount: Scalars["Int"]["output"];
    /** Gets group name. */
    name: Scalars["String"]["output"];
    recommended: Scalars["Boolean"]["output"];
    tenantId: Scalars["String"]["output"];
  };

export type FeaturedUser = FeaturedEntityInterface &
  NodeInterface & {
    __typename?: "FeaturedUser";
    autoPostSubscription: Scalars["Boolean"]["output"];
    autoSubscribe: Scalars["Boolean"]["output"];
    entityGuid: Scalars["String"]["output"];
    id: Scalars["ID"]["output"];
    /** Gets user's display name, or username. */
    name: Scalars["String"]["output"];
    recommended: Scalars["Boolean"]["output"];
    tenantId: Scalars["String"]["output"];
    username?: Maybe<Scalars["String"]["output"]>;
  };

export type FeedExploreTagEdge = EdgeInterface & {
  __typename?: "FeedExploreTagEdge";
  cursor: Scalars["String"]["output"];
  id: Scalars["ID"]["output"];
  node: FeedExploreTagNode;
  type: Scalars["String"]["output"];
};

export type FeedExploreTagNode = NodeInterface & {
  __typename?: "FeedExploreTagNode";
  id: Scalars["ID"]["output"];
  tag: Scalars["String"]["output"];
};

export type FeedHeaderEdge = EdgeInterface & {
  __typename?: "FeedHeaderEdge";
  cursor: Scalars["String"]["output"];
  id: Scalars["ID"]["output"];
  node: FeedHeaderNode;
  type: Scalars["String"]["output"];
};

export type FeedHeaderNode = NodeInterface & {
  __typename?: "FeedHeaderNode";
  id: Scalars["ID"]["output"];
  text: Scalars["String"]["output"];
};

export type FeedHighlightsConnection = ConnectionInterface &
  NodeInterface & {
    __typename?: "FeedHighlightsConnection";
    /** Explicitly will only return activity edges */
    edges: Array<ActivityEdge>;
    id: Scalars["ID"]["output"];
    pageInfo: PageInfo;
  };

export type FeedHighlightsEdge = EdgeInterface & {
  __typename?: "FeedHighlightsEdge";
  cursor: Scalars["String"]["output"];
  id: Scalars["ID"]["output"];
  node: FeedHighlightsConnection;
  type: Scalars["String"]["output"];
};

export type FeedNoticeEdge = EdgeInterface & {
  __typename?: "FeedNoticeEdge";
  cursor: Scalars["String"]["output"];
  node: FeedNoticeNode;
  type: Scalars["String"]["output"];
};

export type FeedNoticeNode = NodeInterface & {
  __typename?: "FeedNoticeNode";
  /** Whether the notice is dismissible */
  dismissible: Scalars["Boolean"]["output"];
  id: Scalars["ID"]["output"];
  /** The key of the notice that the client should render */
  key: Scalars["String"]["output"];
  /** The location in the feed this notice should be displayed. top or inline. */
  location: Scalars["String"]["output"];
};

export type GiftCardBalanceByProductId = {
  __typename?: "GiftCardBalanceByProductId";
  balance: Scalars["Float"]["output"];
  /** Returns the earliest expiring gift that contributes to this balance. */
  earliestExpiringGiftCard?: Maybe<GiftCardNode>;
  productId: GiftCardProductIdEnum;
};

export type GiftCardEdge = EdgeInterface & {
  __typename?: "GiftCardEdge";
  cursor: Scalars["String"]["output"];
  node: GiftCardNode;
};

export type GiftCardNode = NodeInterface & {
  __typename?: "GiftCardNode";
  amount: Scalars["Float"]["output"];
  balance: Scalars["Float"]["output"];
  claimedAt?: Maybe<Scalars["Int"]["output"]>;
  claimedByGuid?: Maybe<Scalars["String"]["output"]>;
  expiresAt: Scalars["Int"]["output"];
  guid?: Maybe<Scalars["String"]["output"]>;
  id: Scalars["ID"]["output"];
  issuedAt: Scalars["Int"]["output"];
  issuedByGuid?: Maybe<Scalars["String"]["output"]>;
  /** Username of the gift card issuer */
  issuedByUsername?: Maybe<Scalars["String"]["output"]>;
  productId: GiftCardProductIdEnum;
  /**
   * Returns transactions relating to the gift card
   * TODO: Find a way to make this not part of the data model
   */
  transactions: GiftCardTransactionsConnection;
};

export type GiftCardNodeTransactionsArgs = {
  after?: InputMaybe<Scalars["String"]["input"]>;
  before?: InputMaybe<Scalars["String"]["input"]>;
  first?: InputMaybe<Scalars["Int"]["input"]>;
  last?: InputMaybe<Scalars["Int"]["input"]>;
};

export enum GiftCardOrderingEnum {
  CreatedAsc = "CREATED_ASC",
  CreatedDesc = "CREATED_DESC",
  ExpiringAsc = "EXPIRING_ASC",
  ExpiringDesc = "EXPIRING_DESC",
}

export enum GiftCardProductIdEnum {
  Boost = "BOOST",
  Plus = "PLUS",
  Pro = "PRO",
  Supermind = "SUPERMIND",
}

export enum GiftCardStatusFilterEnum {
  Active = "ACTIVE",
  Expired = "EXPIRED",
}

export type GiftCardTargetInput = {
  targetEmail?: InputMaybe<Scalars["String"]["input"]>;
  targetUserGuid?: InputMaybe<Scalars["String"]["input"]>;
  targetUsername?: InputMaybe<Scalars["String"]["input"]>;
};

export type GiftCardTransaction = NodeInterface & {
  __typename?: "GiftCardTransaction";
  amount: Scalars["Float"]["output"];
  boostGuid?: Maybe<Scalars["String"]["output"]>;
  createdAt: Scalars["Int"]["output"];
  giftCardGuid?: Maybe<Scalars["String"]["output"]>;
  giftCardIssuerGuid?: Maybe<Scalars["String"]["output"]>;
  giftCardIssuerName?: Maybe<Scalars["String"]["output"]>;
  id: Scalars["ID"]["output"];
  paymentGuid?: Maybe<Scalars["String"]["output"]>;
  refundedAt?: Maybe<Scalars["Int"]["output"]>;
};

export type GiftCardTransactionEdge = EdgeInterface & {
  __typename?: "GiftCardTransactionEdge";
  cursor: Scalars["String"]["output"];
  node: GiftCardTransaction;
};

export type GiftCardTransactionsConnection = ConnectionInterface & {
  __typename?: "GiftCardTransactionsConnection";
  edges: Array<GiftCardTransactionEdge>;
  pageInfo: PageInfo;
};

export type GiftCardsConnection = ConnectionInterface & {
  __typename?: "GiftCardsConnection";
  edges: Array<GiftCardEdge>;
  pageInfo: PageInfo;
};

export type GroupEdge = EdgeInterface & {
  __typename?: "GroupEdge";
  cursor: Scalars["String"]["output"];
  id: Scalars["ID"]["output"];
  node: GroupNode;
  type: Scalars["String"]["output"];
};

export type GroupNode = NodeInterface & {
  __typename?: "GroupNode";
  guid: Scalars["String"]["output"];
  id: Scalars["ID"]["output"];
  legacy: Scalars["String"]["output"];
  nsfw: Array<Scalars["Int"]["output"]>;
  nsfwLock: Array<Scalars["Int"]["output"]>;
  /** Unix timestamp representation of time created */
  timeCreated: Scalars["Int"]["output"];
  /** ISO 8601 timestamp representation of time created */
  timeCreatedISO8601: Scalars["String"]["output"];
  urn: Scalars["String"]["output"];
};

export enum IllegalSubReasonEnum {
  AnimalAbuse = "ANIMAL_ABUSE",
  Extortion = "EXTORTION",
  Fraud = "FRAUD",
  MinorsSexualization = "MINORS_SEXUALIZATION",
  RevengePorn = "REVENGE_PORN",
  Terrorism = "TERRORISM",
  Trafficking = "TRAFFICKING",
}

export type Invite = NodeInterface & {
  __typename?: "Invite";
  bespokeMessage: Scalars["String"]["output"];
  createdTimestamp: Scalars["Int"]["output"];
  email: Scalars["String"]["output"];
  groups?: Maybe<Array<Scalars["Int"]["output"]>>;
  id: Scalars["ID"]["output"];
  inviteId: Scalars["Int"]["output"];
  roles?: Maybe<Array<Role>>;
  sendTimestamp?: Maybe<Scalars["Int"]["output"]>;
  status: InviteEmailStatusEnum;
};

export type InviteConnection = ConnectionInterface &
  NodeInterface & {
    __typename?: "InviteConnection";
    edges: Array<InviteEdge>;
    id: Scalars["ID"]["output"];
    pageInfo: PageInfo;
  };

export type InviteEdge = EdgeInterface & {
  __typename?: "InviteEdge";
  cursor: Scalars["String"]["output"];
  node?: Maybe<Invite>;
};

export enum InviteEmailStatusEnum {
  Accepted = "ACCEPTED",
  Cancelled = "CANCELLED",
  Failed = "FAILED",
  Pending = "PENDING",
  Sending = "SENDING",
  Sent = "SENT",
}

export type KeyValuePairInput = {
  key: Scalars["String"]["input"];
  value: Scalars["String"]["input"];
};

export enum MultiTenantColorScheme {
  Dark = "DARK",
  Light = "LIGHT",
}

export type MultiTenantConfig = {
  __typename?: "MultiTenantConfig";
  colorScheme?: Maybe<MultiTenantColorScheme>;
  communityGuidelines?: Maybe<Scalars["String"]["output"]>;
  lastCacheTimestamp?: Maybe<Scalars["Int"]["output"]>;
  primaryColor?: Maybe<Scalars["String"]["output"]>;
  siteEmail?: Maybe<Scalars["String"]["output"]>;
  siteName?: Maybe<Scalars["String"]["output"]>;
  updatedTimestamp?: Maybe<Scalars["Int"]["output"]>;
};

export type MultiTenantConfigInput = {
  colorScheme?: InputMaybe<MultiTenantColorScheme>;
  communityGuidelines?: InputMaybe<Scalars["String"]["input"]>;
  primaryColor?: InputMaybe<Scalars["String"]["input"]>;
  siteEmail?: InputMaybe<Scalars["String"]["input"]>;
  siteName?: InputMaybe<Scalars["String"]["input"]>;
};

export type MultiTenantDomain = {
  __typename?: "MultiTenantDomain";
  dnsRecord?: Maybe<MultiTenantDomainDnsRecord>;
  domain: Scalars["String"]["output"];
  ownershipVerificationDnsRecord?: Maybe<MultiTenantDomainDnsRecord>;
  status: CustomHostnameStatusEnum;
  tenantId: Scalars["Int"]["output"];
};

export type MultiTenantDomainDnsRecord = {
  __typename?: "MultiTenantDomainDnsRecord";
  name: Scalars["String"]["output"];
  type: DnsRecordEnum;
  value: Scalars["String"]["output"];
};

export type Mutation = {
  __typename?: "Mutation";
  /** Assigns a user to a role */
  assignUserToRole: Role;
  cancelInvite?: Maybe<Scalars["Void"]["output"]>;
  claimGiftCard: GiftCardNode;
  /** Mark an onboarding step for a user as completed. */
  completeOnboardingStep: OnboardingStepProgressState;
  /** Creates a comment on a remote url */
  createEmbeddedComment: CommentEdge;
  createGiftCard: GiftCardNode;
  createMultiTenantDomain: MultiTenantDomain;
  createNetworkRootUser: TenantUser;
  /** Create a new report. */
  createNewReport: Scalars["Boolean"]["output"];
  createRssFeed: RssFeed;
  createTenant: Tenant;
  /** Deletes featured entity. */
  deleteFeaturedEntity: Scalars["Boolean"]["output"];
  /** Dismiss a notice by its key. */
  dismiss: Dismissal;
  invite?: Maybe<Scalars["Void"]["output"]>;
  /** Sets multi-tenant config for the calling tenant. */
  multiTenantConfig: Scalars["Boolean"]["output"];
  /** Provide a verdict for a report. */
  provideVerdict: Scalars["Boolean"]["output"];
  refreshRssFeed: RssFeed;
  removeRssFeed?: Maybe<Scalars["Void"]["output"]>;
  resendInvite?: Maybe<Scalars["Void"]["output"]>;
  /** Creates a comment on a remote url */
  setEmbeddedCommentsSettings: EmbeddedCommentsSettings;
  /** Sets onboarding state for the currently logged in user. */
  setOnboardingState: OnboardingState;
  /** Sets a permission for that a role has */
  setRolePermission: Role;
  /** Stores featured entity. */
  storeFeaturedEntity: FeaturedEntityInterface;
  /** Un-ssigns a user to a role */
  unassignUserFromRole: Scalars["Boolean"]["output"];
  updateAccount: Array<Scalars["String"]["output"]>;
  updatePostSubscription: PostSubscription;
};

export type MutationAssignUserToRoleArgs = {
  roleId: Scalars["Int"]["input"];
  userGuid: Scalars["String"]["input"];
};

export type MutationCancelInviteArgs = {
  inviteId: Scalars["Int"]["input"];
};

export type MutationClaimGiftCardArgs = {
  claimCode: Scalars["String"]["input"];
};

export type MutationCompleteOnboardingStepArgs = {
  additionalData?: InputMaybe<Array<KeyValuePairInput>>;
  stepKey: Scalars["String"]["input"];
  stepType: Scalars["String"]["input"];
};

export type MutationCreateEmbeddedCommentArgs = {
  body: Scalars["String"]["input"];
  ownerGuid: Scalars["String"]["input"];
  parentPath: Scalars["String"]["input"];
  url: Scalars["String"]["input"];
};

export type MutationCreateGiftCardArgs = {
  amount: Scalars["Float"]["input"];
  expiresAt?: InputMaybe<Scalars["Int"]["input"]>;
  productIdEnum: Scalars["Int"]["input"];
  stripePaymentMethodId: Scalars["String"]["input"];
  targetInput: GiftCardTargetInput;
};

export type MutationCreateMultiTenantDomainArgs = {
  hostname: Scalars["String"]["input"];
};

export type MutationCreateNetworkRootUserArgs = {
  networkUser?: InputMaybe<TenantUserInput>;
};

export type MutationCreateNewReportArgs = {
  reportInput: ReportInput;
};

export type MutationCreateRssFeedArgs = {
  rssFeed: RssFeedInput;
};

export type MutationCreateTenantArgs = {
  tenant?: InputMaybe<TenantInput>;
};

export type MutationDeleteFeaturedEntityArgs = {
  entityGuid: Scalars["String"]["input"];
};

export type MutationDismissArgs = {
  key: Scalars["String"]["input"];
};

export type MutationInviteArgs = {
  bespokeMessage: Scalars["String"]["input"];
  emails: Scalars["String"]["input"];
  groups?: InputMaybe<Array<Scalars["Int"]["input"]>>;
  roles?: InputMaybe<Array<Scalars["Int"]["input"]>>;
};

export type MutationMultiTenantConfigArgs = {
  multiTenantConfigInput: MultiTenantConfigInput;
};

export type MutationProvideVerdictArgs = {
  verdictInput: VerdictInput;
};

export type MutationRefreshRssFeedArgs = {
  feedId: Scalars["String"]["input"];
};

export type MutationRemoveRssFeedArgs = {
  feedId: Scalars["String"]["input"];
};

export type MutationResendInviteArgs = {
  inviteId: Scalars["Int"]["input"];
};

export type MutationSetEmbeddedCommentsSettingsArgs = {
  autoImportsEnabled: Scalars["Boolean"]["input"];
  domain: Scalars["String"]["input"];
  pathRegex: Scalars["String"]["input"];
};

export type MutationSetOnboardingStateArgs = {
  completed: Scalars["Boolean"]["input"];
};

export type MutationSetRolePermissionArgs = {
  enabled?: InputMaybe<Scalars["Boolean"]["input"]>;
  permission: PermissionsEnum;
  roleId: Scalars["Int"]["input"];
};

export type MutationStoreFeaturedEntityArgs = {
  featuredEntity: FeaturedEntityInput;
};

export type MutationUnassignUserFromRoleArgs = {
  roleId: Scalars["Int"]["input"];
  userGuid: Scalars["String"]["input"];
};

export type MutationUpdateAccountArgs = {
  currentUsername: Scalars["String"]["input"];
  newEmail?: InputMaybe<Scalars["String"]["input"]>;
  newUsername?: InputMaybe<Scalars["String"]["input"]>;
  resetMFA?: InputMaybe<Scalars["Boolean"]["input"]>;
};

export type MutationUpdatePostSubscriptionArgs = {
  entityGuid: Scalars["String"]["input"];
  frequency: PostSubscriptionFrequencyEnum;
};

export type NewsfeedConnection = ConnectionInterface & {
  __typename?: "NewsfeedConnection";
  edges: Array<EdgeInterface>;
  pageInfo: PageInfo;
};

export type NodeImpl = NodeInterface & {
  __typename?: "NodeImpl";
  id: Scalars["ID"]["output"];
};

export type NodeInterface = {
  id: Scalars["ID"]["output"];
};

export enum NsfwSubReasonEnum {
  Nudity = "NUDITY",
  Pornography = "PORNOGRAPHY",
  Profanity = "PROFANITY",
  RaceReligionGender = "RACE_RELIGION_GENDER",
  ViolenceGore = "VIOLENCE_GORE",
}

export type OidcProviderPublic = {
  __typename?: "OidcProviderPublic";
  clientId: Scalars["String"]["output"];
  id: Scalars["Int"]["output"];
  issuer: Scalars["String"]["output"];
  loginUrl: Scalars["String"]["output"];
  name: Scalars["String"]["output"];
};

export type OnboardingState = {
  __typename?: "OnboardingState";
  completedAt?: Maybe<Scalars["Int"]["output"]>;
  startedAt: Scalars["Int"]["output"];
  userGuid?: Maybe<Scalars["String"]["output"]>;
};

export type OnboardingStepProgressState = {
  __typename?: "OnboardingStepProgressState";
  completedAt?: Maybe<Scalars["Int"]["output"]>;
  stepKey: Scalars["String"]["output"];
  stepType: Scalars["String"]["output"];
  userGuid?: Maybe<Scalars["String"]["output"]>;
};

export type PageInfo = {
  __typename?: "PageInfo";
  endCursor?: Maybe<Scalars["String"]["output"]>;
  hasNextPage: Scalars["Boolean"]["output"];
  hasPreviousPage: Scalars["Boolean"]["output"];
  startCursor?: Maybe<Scalars["String"]["output"]>;
};

export type PaymentMethod = {
  __typename?: "PaymentMethod";
  balance?: Maybe<Scalars["Float"]["output"]>;
  id: Scalars["String"]["output"];
  name: Scalars["String"]["output"];
};

export enum PermissionsEnum {
  CanAssignPermissions = "CAN_ASSIGN_PERMISSIONS",
  CanBoost = "CAN_BOOST",
  CanComment = "CAN_COMMENT",
  CanCreateGroup = "CAN_CREATE_GROUP",
  CanCreatePost = "CAN_CREATE_POST",
  CanInteract = "CAN_INTERACT",
  CanUploadVideo = "CAN_UPLOAD_VIDEO",
}

export type Plan = {
  __typename?: "Plan";
  description: Scalars["String"]["output"];
  id: Scalars["String"]["output"];
  monthlyFeeCents: Scalars["Int"]["output"];
  name: Scalars["String"]["output"];
  oneTimeFeeCents?: Maybe<Scalars["Int"]["output"]>;
  perks: Array<Scalars["String"]["output"]>;
  perksTitle: Scalars["String"]["output"];
};

export type PlanSummary = {
  __typename?: "PlanSummary";
  id: Scalars["String"]["output"];
  monthlyFeeCents: Scalars["Int"]["output"];
  name: Scalars["String"]["output"];
  oneTimeFeeCents?: Maybe<Scalars["Int"]["output"]>;
};

export type PostSubscription = {
  __typename?: "PostSubscription";
  entityGuid: Scalars["String"]["output"];
  frequency: PostSubscriptionFrequencyEnum;
  userGuid: Scalars["String"]["output"];
};

export enum PostSubscriptionFrequencyEnum {
  Always = "ALWAYS",
  Highlights = "HIGHLIGHTS",
  Never = "NEVER",
}

export type PublisherRecsConnection = ConnectionInterface &
  NodeInterface & {
    __typename?: "PublisherRecsConnection";
    dismissible: Scalars["Boolean"]["output"];
    /**
     * TODO: clean this up to help with typing. Union types wont work due to the following error being outputted
     * `Error: ConnectionInterface.edges expects type "[EdgeInterface!]!" but PublisherRecsConnection.edges provides type "[UnionUserEdgeBoostEdge!]!".`
     */
    edges: Array<EdgeInterface>;
    id: Scalars["ID"]["output"];
    pageInfo: PageInfo;
  };

export type PublisherRecsEdge = EdgeInterface & {
  __typename?: "PublisherRecsEdge";
  cursor: Scalars["String"]["output"];
  id: Scalars["ID"]["output"];
  node: PublisherRecsConnection;
  type: Scalars["String"]["output"];
};

export type Query = {
  __typename?: "Query";
  activity: ActivityNode;
  /** Returns all permissions that exist on the site */
  allPermissions: Array<PermissionsEnum>;
  /** Returns all roles that exist on the site and their permission assignments */
  allRoles: Array<Role>;
  /** Returns the permissions that the current session holds */
  assignedPermissions: Array<PermissionsEnum>;
  /** Returns the roles the session holds */
  assignedRoles: Array<Role>;
  /** Gets Boosts. */
  boosts: BoostsConnection;
  checkoutLink: Scalars["String"]["output"];
  checkoutPage: CheckoutPage;
  /** Get dismissal by key. */
  dismissalByKey?: Maybe<Dismissal>;
  /** Get all of a users dismissals. */
  dismissals: Array<Dismissal>;
  /**
   * Returns comments to be shown in the embedded comments app.
   * The comments will be associated with an activity post. If the activity post
   * does not exist, we will attempt to create it
   */
  embeddedComments: EmbeddedCommentsConnection;
  /** Returns the configured embedded-comments plugin settings for a user */
  embeddedCommentsSettings?: Maybe<EmbeddedCommentsSettings>;
  /** Gets featured entities. */
  featuredEntities: FeaturedEntityConnection;
  /** Returns an individual gift card */
  giftCard: GiftCardNode;
  /** Returns an individual gift card by its claim code. */
  giftCardByClaimCode: GiftCardNode;
  /**
   * Returns a list of gift card transactions for a ledger,
   * containing more information than just getting transactions,
   * including linked boost_guid's for Boost payments and injects
   * a transaction for the initial deposit.
   */
  giftCardTransactionLedger: GiftCardTransactionsConnection;
  /** Returns a list of gift card transactions */
  giftCardTransactions: GiftCardTransactionsConnection;
  /** Returns a list of gift cards belonging to a user */
  giftCards: GiftCardsConnection;
  /** The available balance a user has */
  giftCardsBalance: Scalars["Float"]["output"];
  /** The available balances of each gift card types */
  giftCardsBalances: Array<GiftCardBalanceByProductId>;
  invite: Invite;
  invites: InviteConnection;
  /** Gets multi-tenant config for the calling tenant. */
  multiTenantConfig?: Maybe<MultiTenantConfig>;
  multiTenantDomain: MultiTenantDomain;
  newsfeed: NewsfeedConnection;
  oidcProviders: Array<OidcProviderPublic>;
  /** Gets onboarding state for the currently logged in user. */
  onboardingState?: Maybe<OnboardingState>;
  /** Get the currently logged in users onboarding step progress. */
  onboardingStepProgress: Array<OnboardingStepProgressState>;
  /** Get a list of payment methods for the logged in user */
  paymentMethods: Array<PaymentMethod>;
  postSubscription: PostSubscription;
  /** Gets reports. */
  reports: ReportsConnection;
  rssFeed: RssFeed;
  rssFeeds: Array<RssFeed>;
  search: SearchResultsConnection;
  tenantAssets: AssetConnection;
  tenantQuotaUsage: QuotaDetails;
  tenants: Array<Tenant>;
  userAssets: AssetConnection;
  userQuotaUsage: QuotaDetails;
  /** Returns users and their roles */
  usersByRole: UserRoleConnection;
};

export type QueryActivityArgs = {
  guid: Scalars["String"]["input"];
};

export type QueryAssignedRolesArgs = {
  userGuid?: InputMaybe<Scalars["String"]["input"]>;
};

export type QueryBoostsArgs = {
  after?: InputMaybe<Scalars["Int"]["input"]>;
  before?: InputMaybe<Scalars["Int"]["input"]>;
  first?: InputMaybe<Scalars["Int"]["input"]>;
  last?: InputMaybe<Scalars["Int"]["input"]>;
  servedByGuid?: InputMaybe<Scalars["String"]["input"]>;
  source?: InputMaybe<Scalars["String"]["input"]>;
  targetAudience?: InputMaybe<Scalars["Int"]["input"]>;
  targetLocation?: InputMaybe<Scalars["Int"]["input"]>;
};

export type QueryCheckoutLinkArgs = {
  addOnIds?: InputMaybe<Array<Scalars["String"]["input"]>>;
  planId: Scalars["String"]["input"];
  timePeriod: CheckoutTimePeriodEnum;
};

export type QueryCheckoutPageArgs = {
  addOnIds?: InputMaybe<Array<Scalars["String"]["input"]>>;
  page: CheckoutPageKeyEnum;
  planId: Scalars["String"]["input"];
  timePeriod: CheckoutTimePeriodEnum;
};

export type QueryDismissalByKeyArgs = {
  key: Scalars["String"]["input"];
};

export type QueryEmbeddedCommentsArgs = {
  after?: InputMaybe<Scalars["String"]["input"]>;
  before?: InputMaybe<Scalars["String"]["input"]>;
  first?: InputMaybe<Scalars["Int"]["input"]>;
  last?: InputMaybe<Scalars["Int"]["input"]>;
  ownerGuid: Scalars["String"]["input"];
  parentPath?: InputMaybe<Scalars["String"]["input"]>;
  url: Scalars["String"]["input"];
};

export type QueryFeaturedEntitiesArgs = {
  after?: InputMaybe<Scalars["Int"]["input"]>;
  first?: InputMaybe<Scalars["Int"]["input"]>;
  type: FeaturedEntityTypeEnum;
};

export type QueryGiftCardArgs = {
  guid: Scalars["String"]["input"];
};

export type QueryGiftCardByClaimCodeArgs = {
  claimCode: Scalars["String"]["input"];
};

export type QueryGiftCardTransactionLedgerArgs = {
  after?: InputMaybe<Scalars["String"]["input"]>;
  before?: InputMaybe<Scalars["String"]["input"]>;
  first?: InputMaybe<Scalars["Int"]["input"]>;
  giftCardGuid: Scalars["String"]["input"];
  last?: InputMaybe<Scalars["Int"]["input"]>;
};

export type QueryGiftCardTransactionsArgs = {
  after?: InputMaybe<Scalars["String"]["input"]>;
  before?: InputMaybe<Scalars["String"]["input"]>;
  first?: InputMaybe<Scalars["Int"]["input"]>;
  last?: InputMaybe<Scalars["Int"]["input"]>;
};

export type QueryGiftCardsArgs = {
  after?: InputMaybe<Scalars["String"]["input"]>;
  before?: InputMaybe<Scalars["String"]["input"]>;
  first?: InputMaybe<Scalars["Int"]["input"]>;
  includeIssued?: InputMaybe<Scalars["Boolean"]["input"]>;
  last?: InputMaybe<Scalars["Int"]["input"]>;
  ordering?: InputMaybe<GiftCardOrderingEnum>;
  productId?: InputMaybe<GiftCardProductIdEnum>;
  statusFilter?: InputMaybe<GiftCardStatusFilterEnum>;
};

export type QueryInviteArgs = {
  inviteId: Scalars["Int"]["input"];
};

export type QueryInvitesArgs = {
  after?: InputMaybe<Scalars["String"]["input"]>;
  first: Scalars["Int"]["input"];
  search?: InputMaybe<Scalars["String"]["input"]>;
};

export type QueryNewsfeedArgs = {
  after?: InputMaybe<Scalars["String"]["input"]>;
  algorithm: Scalars["String"]["input"];
  before?: InputMaybe<Scalars["String"]["input"]>;
  first?: InputMaybe<Scalars["Int"]["input"]>;
  inFeedNoticesDelivered?: InputMaybe<Array<Scalars["String"]["input"]>>;
  last?: InputMaybe<Scalars["Int"]["input"]>;
};

export type QueryPaymentMethodsArgs = {
  productId?: InputMaybe<GiftCardProductIdEnum>;
};

export type QueryPostSubscriptionArgs = {
  entityGuid: Scalars["String"]["input"];
};

export type QueryReportsArgs = {
  after?: InputMaybe<Scalars["Int"]["input"]>;
  first?: InputMaybe<Scalars["Int"]["input"]>;
  status?: InputMaybe<ReportStatusEnum>;
};

export type QueryRssFeedArgs = {
  feedId: Scalars["String"]["input"];
};

export type QuerySearchArgs = {
  after?: InputMaybe<Scalars["String"]["input"]>;
  before?: InputMaybe<Scalars["String"]["input"]>;
  filter: SearchFilterEnum;
  first?: InputMaybe<Scalars["Int"]["input"]>;
  last?: InputMaybe<Scalars["Int"]["input"]>;
  mediaType: SearchMediaTypeEnum;
  nsfw?: InputMaybe<Array<SearchNsfwEnum>>;
  query: Scalars["String"]["input"];
};

export type QueryTenantAssetsArgs = {
  after?: InputMaybe<Scalars["String"]["input"]>;
  before?: InputMaybe<Scalars["String"]["input"]>;
  first?: InputMaybe<Scalars["Int"]["input"]>;
};

export type QueryTenantsArgs = {
  first?: InputMaybe<Scalars["Int"]["input"]>;
  last?: InputMaybe<Scalars["Int"]["input"]>;
};

export type QueryUserAssetsArgs = {
  after?: InputMaybe<Scalars["String"]["input"]>;
  before?: InputMaybe<Scalars["String"]["input"]>;
  first?: InputMaybe<Scalars["Int"]["input"]>;
};

export type QueryUsersByRoleArgs = {
  after?: InputMaybe<Scalars["String"]["input"]>;
  first?: InputMaybe<Scalars["Int"]["input"]>;
  roleId?: InputMaybe<Scalars["Int"]["input"]>;
};

export type QuotaDetails = {
  __typename?: "QuotaDetails";
  sizeInBytes: Scalars["Int"]["output"];
};

export type Report = NodeInterface & {
  __typename?: "Report";
  action?: Maybe<ReportActionEnum>;
  createdTimestamp: Scalars["Int"]["output"];
  cursor?: Maybe<Scalars["String"]["output"]>;
  /** Gets entity edge from entityUrn. */
  entityEdge?: Maybe<UnionActivityEdgeUserEdgeGroupEdgeCommentEdge>;
  entityGuid?: Maybe<Scalars["String"]["output"]>;
  entityUrn: Scalars["String"]["output"];
  /** Gets ID for GraphQL. */
  id: Scalars["ID"]["output"];
  illegalSubReason?: Maybe<IllegalSubReasonEnum>;
  moderatedByGuid?: Maybe<Scalars["String"]["output"]>;
  nsfwSubReason?: Maybe<NsfwSubReasonEnum>;
  reason: ReportReasonEnum;
  reportGuid?: Maybe<Scalars["String"]["output"]>;
  reportedByGuid?: Maybe<Scalars["String"]["output"]>;
  /** Gets reported user edge from reportedByGuid. */
  reportedByUserEdge?: Maybe<UserEdge>;
  securitySubReason?: Maybe<SecuritySubReasonEnum>;
  status: ReportStatusEnum;
  tenantId?: Maybe<Scalars["String"]["output"]>;
  updatedTimestamp?: Maybe<Scalars["Int"]["output"]>;
};

export enum ReportActionEnum {
  Ban = "BAN",
  Delete = "DELETE",
  Ignore = "IGNORE",
}

export type ReportEdge = EdgeInterface & {
  __typename?: "ReportEdge";
  /** Gets cursor for GraphQL. */
  cursor: Scalars["String"]["output"];
  /** Gets ID for GraphQL. */
  id: Scalars["ID"]["output"];
  /** Gets node. */
  node?: Maybe<Report>;
  /** Gets type for GraphQL. */
  type: Scalars["String"]["output"];
};

export type ReportInput = {
  entityUrn: Scalars["String"]["input"];
  illegalSubReason?: InputMaybe<IllegalSubReasonEnum>;
  nsfwSubReason?: InputMaybe<NsfwSubReasonEnum>;
  reason: ReportReasonEnum;
  securitySubReason?: InputMaybe<SecuritySubReasonEnum>;
};

export enum ReportReasonEnum {
  ActivityPubReport = "ACTIVITY_PUB_REPORT",
  AnotherReason = "ANOTHER_REASON",
  Harassment = "HARASSMENT",
  Illegal = "ILLEGAL",
  Impersonation = "IMPERSONATION",
  InauthenticEngagement = "INAUTHENTIC_ENGAGEMENT",
  IncitementToViolence = "INCITEMENT_TO_VIOLENCE",
  IntellectualPropertyViolation = "INTELLECTUAL_PROPERTY_VIOLATION",
  Malware = "MALWARE",
  Nsfw = "NSFW",
  PersonalConfidentialInformation = "PERSONAL_CONFIDENTIAL_INFORMATION",
  Security = "SECURITY",
  Spam = "SPAM",
  ViolatesPremiumContentPolicy = "VIOLATES_PREMIUM_CONTENT_POLICY",
}

export enum ReportStatusEnum {
  Actioned = "ACTIONED",
  Pending = "PENDING",
}

export type ReportsConnection = ConnectionInterface & {
  __typename?: "ReportsConnection";
  /** Gets connections edges. */
  edges: Array<EdgeInterface>;
  /** ID for GraphQL. */
  id: Scalars["ID"]["output"];
  pageInfo: PageInfo;
};

export type Role = {
  __typename?: "Role";
  id: Scalars["Int"]["output"];
  name: Scalars["String"]["output"];
  permissions: Array<PermissionsEnum>;
};

export type RssFeed = {
  __typename?: "RssFeed";
  createdAtTimestamp?: Maybe<Scalars["Int"]["output"]>;
  feedId: Scalars["String"]["output"];
  lastFetchAtTimestamp?: Maybe<Scalars["Int"]["output"]>;
  lastFetchStatus?: Maybe<RssFeedLastFetchStatusEnum>;
  tenantId?: Maybe<Scalars["Int"]["output"]>;
  title: Scalars["String"]["output"];
  url: Scalars["String"]["output"];
  userGuid: Scalars["String"]["output"];
};

export type RssFeedInput = {
  url: Scalars["String"]["input"];
};

export enum RssFeedLastFetchStatusEnum {
  FailedToConnect = "FAILED_TO_CONNECT",
  FailedToParse = "FAILED_TO_PARSE",
  FetchInProgress = "FETCH_IN_PROGRESS",
  Success = "SUCCESS",
}

export enum SearchFilterEnum {
  Group = "GROUP",
  Latest = "LATEST",
  Top = "TOP",
  User = "USER",
}

export enum SearchMediaTypeEnum {
  All = "ALL",
  Blog = "BLOG",
  Image = "IMAGE",
  Video = "VIDEO",
}

export enum SearchNsfwEnum {
  Nudity = "NUDITY",
  Other = "OTHER",
  Pornography = "PORNOGRAPHY",
  Profanity = "PROFANITY",
  RaceReligion = "RACE_RELIGION",
  Violence = "VIOLENCE",
}

export type SearchResultsConnection = ConnectionInterface & {
  __typename?: "SearchResultsConnection";
  /** The number of search records matching the query */
  count: Scalars["Int"]["output"];
  edges: Array<EdgeInterface>;
  pageInfo: PageInfo;
};

export type SearchResultsCount = {
  __typename?: "SearchResultsCount";
  count: Scalars["Int"]["output"];
};

export enum SecuritySubReasonEnum {
  HackedAccount = "HACKED_ACCOUNT",
}

export type Summary = {
  __typename?: "Summary";
  addonsSummary: Array<AddOn>;
  planSummary: PlanSummary;
  totalInitialFeeCents: Scalars["Int"]["output"];
  totalMonthlyFeeCents: Scalars["Int"]["output"];
};

export type Tenant = {
  __typename?: "Tenant";
  config?: Maybe<MultiTenantConfig>;
  domain?: Maybe<Scalars["String"]["output"]>;
  id: Scalars["Int"]["output"];
  ownerGuid?: Maybe<Scalars["String"]["output"]>;
  rootUserGuid?: Maybe<Scalars["String"]["output"]>;
};

export type TenantInput = {
  config?: InputMaybe<MultiTenantConfigInput>;
  domain?: InputMaybe<Scalars["String"]["input"]>;
  ownerGuid?: InputMaybe<Scalars["Int"]["input"]>;
};

export type TenantUser = {
  __typename?: "TenantUser";
  guid: Scalars["String"]["output"];
  role: TenantUserRoleEnum;
  tenantId: Scalars["Int"]["output"];
  username: Scalars["String"]["output"];
};

export type TenantUserInput = {
  tenantId?: InputMaybe<Scalars["Int"]["input"]>;
  username?: InputMaybe<Scalars["String"]["input"]>;
};

export enum TenantUserRoleEnum {
  Admin = "ADMIN",
  Owner = "OWNER",
  User = "USER",
}

export type UnionActivityEdgeUserEdgeGroupEdgeCommentEdge =
  | ActivityEdge
  | CommentEdge
  | GroupEdge
  | UserEdge;

export type UserEdge = EdgeInterface & {
  __typename?: "UserEdge";
  cursor: Scalars["String"]["output"];
  id: Scalars["ID"]["output"];
  node: UserNode;
  type: Scalars["String"]["output"];
};

export type UserNode = NodeInterface & {
  __typename?: "UserNode";
  briefDescription: Scalars["String"]["output"];
  /** The users public ETH address */
  ethAddress?: Maybe<Scalars["String"]["output"]>;
  guid: Scalars["String"]["output"];
  iconUrl: Scalars["String"]["output"];
  id: Scalars["ID"]["output"];
  /** The number of views the users has received. Includes views from their posts */
  impressionsCount: Scalars["Int"]["output"];
  /** The user is a founder (contributed to crowdfunding) */
  isFounder: Scalars["Boolean"]["output"];
  /** The user is a member of Minds+ */
  isPlus: Scalars["Boolean"]["output"];
  /** The user is a member of Minds Pro */
  isPro: Scalars["Boolean"]["output"];
  /** You are subscribed to this user */
  isSubscribed: Scalars["Boolean"]["output"];
  /** The user is subscribed to you */
  isSubscriber: Scalars["Boolean"]["output"];
  /** The user is a verified */
  isVerified: Scalars["Boolean"]["output"];
  legacy: Scalars["String"]["output"];
  name: Scalars["String"]["output"];
  nsfw: Array<Scalars["Int"]["output"]>;
  nsfwLock: Array<Scalars["Int"]["output"]>;
  /** The number of subscribers the user has */
  subscribersCount: Scalars["Int"]["output"];
  /** The number of channels the user is subscribed to */
  subscriptionsCount: Scalars["Int"]["output"];
  /** Unix timestamp representation of time created */
  timeCreated: Scalars["Int"]["output"];
  /** ISO 8601 timestamp representation of time created */
  timeCreatedISO8601: Scalars["String"]["output"];
  urn: Scalars["String"]["output"];
  username: Scalars["String"]["output"];
};

export type UserRoleConnection = ConnectionInterface & {
  __typename?: "UserRoleConnection";
  edges: Array<UserRoleEdge>;
  pageInfo: PageInfo;
};

export type UserRoleEdge = EdgeInterface & {
  __typename?: "UserRoleEdge";
  cursor: Scalars["String"]["output"];
  node: UserNode;
  roles: Array<Role>;
};

export type VerdictInput = {
  action: ReportActionEnum;
  reportGuid?: InputMaybe<Scalars["String"]["input"]>;
};

export type CreateCommentMutationVariables = Exact<{
  ownerGuid: Scalars["String"]["input"];
  url: Scalars["String"]["input"];
  parentPath: Scalars["String"]["input"];
  body: Scalars["String"]["input"];
}>;

export type CreateCommentMutation = {
  __typename?: "Mutation";
  createEmbeddedComment: { __typename?: "CommentEdge"; id: string } & {
    " $fragmentRefs"?: { CommonCommentEdgeFragment: CommonCommentEdgeFragment };
  };
};

export type CommonCommentEdgeFragment = {
  __typename?: "CommentEdge";
  id: string;
  repliesCount: number;
  hasVotedUp: boolean;
  hasVotedDown: boolean;
  votesUpCount: number;
  node: {
    __typename?: "CommentNode";
    id: string;
    luid: string;
    parentPath: string;
    childPath: string;
    guid: string;
    body: string;
    timeCreated: number;
    url: string;
    attachment?: {
      __typename?: "AttachmentNode";
      type: string;
      src: string;
    } | null;
    owner: {
      __typename?: "UserNode";
      guid: string;
      name: string;
      iconUrl: string;
    };
  };
} & { " $fragmentName"?: "CommonCommentEdgeFragment" };

export type GetEmbeddedCommentsQueryVariables = Exact<{
  ownerGuid: Scalars["String"]["input"];
  url: Scalars["String"]["input"];
  parentPath?: InputMaybe<Scalars["String"]["input"]>;
  limit?: InputMaybe<Scalars["Int"]["input"]>;
}>;

export type GetEmbeddedCommentsQuery = {
  __typename?: "Query";
  embeddedComments: {
    __typename?: "EmbeddedCommentsConnection";
    totalCount: number;
    activityUrl: string;
    edges: Array<
      { __typename?: "CommentEdge"; id: string } & {
        " $fragmentRefs"?: {
          CommonCommentEdgeFragment: CommonCommentEdgeFragment;
        };
      }
    >;
    pageInfo: {
      __typename?: "PageInfo";
      hasPreviousPage: boolean;
      hasNextPage: boolean;
      startCursor?: string | null;
      endCursor?: string | null;
    };
  };
};

export const CommonCommentEdgeFragmentDoc = {
  kind: "Document",
  definitions: [
    {
      kind: "FragmentDefinition",
      name: { kind: "Name", value: "CommonCommentEdge" },
      typeCondition: {
        kind: "NamedType",
        name: { kind: "Name", value: "CommentEdge" },
      },
      selectionSet: {
        kind: "SelectionSet",
        selections: [
          { kind: "Field", name: { kind: "Name", value: "id" } },
          {
            kind: "Field",
            name: { kind: "Name", value: "node" },
            selectionSet: {
              kind: "SelectionSet",
              selections: [
                { kind: "Field", name: { kind: "Name", value: "id" } },
                { kind: "Field", name: { kind: "Name", value: "luid" } },
                { kind: "Field", name: { kind: "Name", value: "parentPath" } },
                { kind: "Field", name: { kind: "Name", value: "childPath" } },
                { kind: "Field", name: { kind: "Name", value: "guid" } },
                { kind: "Field", name: { kind: "Name", value: "body" } },
                { kind: "Field", name: { kind: "Name", value: "timeCreated" } },
                { kind: "Field", name: { kind: "Name", value: "url" } },
                {
                  kind: "Field",
                  name: { kind: "Name", value: "attachment" },
                  selectionSet: {
                    kind: "SelectionSet",
                    selections: [
                      { kind: "Field", name: { kind: "Name", value: "type" } },
                      { kind: "Field", name: { kind: "Name", value: "src" } },
                    ],
                  },
                },
                {
                  kind: "Field",
                  name: { kind: "Name", value: "owner" },
                  selectionSet: {
                    kind: "SelectionSet",
                    selections: [
                      { kind: "Field", name: { kind: "Name", value: "guid" } },
                      { kind: "Field", name: { kind: "Name", value: "name" } },
                      {
                        kind: "Field",
                        name: { kind: "Name", value: "iconUrl" },
                      },
                    ],
                  },
                },
              ],
            },
          },
          { kind: "Field", name: { kind: "Name", value: "repliesCount" } },
          { kind: "Field", name: { kind: "Name", value: "hasVotedUp" } },
          { kind: "Field", name: { kind: "Name", value: "hasVotedDown" } },
          { kind: "Field", name: { kind: "Name", value: "votesUpCount" } },
        ],
      },
    },
  ],
} as unknown as DocumentNode<CommonCommentEdgeFragment, unknown>;
export const CreateCommentDocument = {
  kind: "Document",
  definitions: [
    {
      kind: "OperationDefinition",
      operation: "mutation",
      name: { kind: "Name", value: "CreateComment" },
      variableDefinitions: [
        {
          kind: "VariableDefinition",
          variable: {
            kind: "Variable",
            name: { kind: "Name", value: "ownerGuid" },
          },
          type: {
            kind: "NonNullType",
            type: {
              kind: "NamedType",
              name: { kind: "Name", value: "String" },
            },
          },
        },
        {
          kind: "VariableDefinition",
          variable: { kind: "Variable", name: { kind: "Name", value: "url" } },
          type: {
            kind: "NonNullType",
            type: {
              kind: "NamedType",
              name: { kind: "Name", value: "String" },
            },
          },
        },
        {
          kind: "VariableDefinition",
          variable: {
            kind: "Variable",
            name: { kind: "Name", value: "parentPath" },
          },
          type: {
            kind: "NonNullType",
            type: {
              kind: "NamedType",
              name: { kind: "Name", value: "String" },
            },
          },
        },
        {
          kind: "VariableDefinition",
          variable: { kind: "Variable", name: { kind: "Name", value: "body" } },
          type: {
            kind: "NonNullType",
            type: {
              kind: "NamedType",
              name: { kind: "Name", value: "String" },
            },
          },
        },
      ],
      selectionSet: {
        kind: "SelectionSet",
        selections: [
          {
            kind: "Field",
            name: { kind: "Name", value: "createEmbeddedComment" },
            arguments: [
              {
                kind: "Argument",
                name: { kind: "Name", value: "ownerGuid" },
                value: {
                  kind: "Variable",
                  name: { kind: "Name", value: "ownerGuid" },
                },
              },
              {
                kind: "Argument",
                name: { kind: "Name", value: "url" },
                value: {
                  kind: "Variable",
                  name: { kind: "Name", value: "url" },
                },
              },
              {
                kind: "Argument",
                name: { kind: "Name", value: "parentPath" },
                value: {
                  kind: "Variable",
                  name: { kind: "Name", value: "parentPath" },
                },
              },
              {
                kind: "Argument",
                name: { kind: "Name", value: "body" },
                value: {
                  kind: "Variable",
                  name: { kind: "Name", value: "body" },
                },
              },
            ],
            selectionSet: {
              kind: "SelectionSet",
              selections: [
                { kind: "Field", name: { kind: "Name", value: "id" } },
                {
                  kind: "FragmentSpread",
                  name: { kind: "Name", value: "CommonCommentEdge" },
                },
              ],
            },
          },
        ],
      },
    },
    {
      kind: "FragmentDefinition",
      name: { kind: "Name", value: "CommonCommentEdge" },
      typeCondition: {
        kind: "NamedType",
        name: { kind: "Name", value: "CommentEdge" },
      },
      selectionSet: {
        kind: "SelectionSet",
        selections: [
          { kind: "Field", name: { kind: "Name", value: "id" } },
          {
            kind: "Field",
            name: { kind: "Name", value: "node" },
            selectionSet: {
              kind: "SelectionSet",
              selections: [
                { kind: "Field", name: { kind: "Name", value: "id" } },
                { kind: "Field", name: { kind: "Name", value: "luid" } },
                { kind: "Field", name: { kind: "Name", value: "parentPath" } },
                { kind: "Field", name: { kind: "Name", value: "childPath" } },
                { kind: "Field", name: { kind: "Name", value: "guid" } },
                { kind: "Field", name: { kind: "Name", value: "body" } },
                { kind: "Field", name: { kind: "Name", value: "timeCreated" } },
                { kind: "Field", name: { kind: "Name", value: "url" } },
                {
                  kind: "Field",
                  name: { kind: "Name", value: "attachment" },
                  selectionSet: {
                    kind: "SelectionSet",
                    selections: [
                      { kind: "Field", name: { kind: "Name", value: "type" } },
                      { kind: "Field", name: { kind: "Name", value: "src" } },
                    ],
                  },
                },
                {
                  kind: "Field",
                  name: { kind: "Name", value: "owner" },
                  selectionSet: {
                    kind: "SelectionSet",
                    selections: [
                      { kind: "Field", name: { kind: "Name", value: "guid" } },
                      { kind: "Field", name: { kind: "Name", value: "name" } },
                      {
                        kind: "Field",
                        name: { kind: "Name", value: "iconUrl" },
                      },
                    ],
                  },
                },
              ],
            },
          },
          { kind: "Field", name: { kind: "Name", value: "repliesCount" } },
          { kind: "Field", name: { kind: "Name", value: "hasVotedUp" } },
          { kind: "Field", name: { kind: "Name", value: "hasVotedDown" } },
          { kind: "Field", name: { kind: "Name", value: "votesUpCount" } },
        ],
      },
    },
  ],
} as unknown as DocumentNode<
  CreateCommentMutation,
  CreateCommentMutationVariables
>;
export const GetEmbeddedCommentsDocument = {
  kind: "Document",
  definitions: [
    {
      kind: "OperationDefinition",
      operation: "query",
      name: { kind: "Name", value: "GetEmbeddedComments" },
      variableDefinitions: [
        {
          kind: "VariableDefinition",
          variable: {
            kind: "Variable",
            name: { kind: "Name", value: "ownerGuid" },
          },
          type: {
            kind: "NonNullType",
            type: {
              kind: "NamedType",
              name: { kind: "Name", value: "String" },
            },
          },
        },
        {
          kind: "VariableDefinition",
          variable: { kind: "Variable", name: { kind: "Name", value: "url" } },
          type: {
            kind: "NonNullType",
            type: {
              kind: "NamedType",
              name: { kind: "Name", value: "String" },
            },
          },
        },
        {
          kind: "VariableDefinition",
          variable: {
            kind: "Variable",
            name: { kind: "Name", value: "parentPath" },
          },
          type: { kind: "NamedType", name: { kind: "Name", value: "String" } },
        },
        {
          kind: "VariableDefinition",
          variable: {
            kind: "Variable",
            name: { kind: "Name", value: "limit" },
          },
          type: { kind: "NamedType", name: { kind: "Name", value: "Int" } },
        },
      ],
      selectionSet: {
        kind: "SelectionSet",
        selections: [
          {
            kind: "Field",
            name: { kind: "Name", value: "embeddedComments" },
            arguments: [
              {
                kind: "Argument",
                name: { kind: "Name", value: "ownerGuid" },
                value: {
                  kind: "Variable",
                  name: { kind: "Name", value: "ownerGuid" },
                },
              },
              {
                kind: "Argument",
                name: { kind: "Name", value: "url" },
                value: {
                  kind: "Variable",
                  name: { kind: "Name", value: "url" },
                },
              },
              {
                kind: "Argument",
                name: { kind: "Name", value: "parentPath" },
                value: {
                  kind: "Variable",
                  name: { kind: "Name", value: "parentPath" },
                },
              },
              {
                kind: "Argument",
                name: { kind: "Name", value: "first" },
                value: {
                  kind: "Variable",
                  name: { kind: "Name", value: "limit" },
                },
              },
            ],
            selectionSet: {
              kind: "SelectionSet",
              selections: [
                { kind: "Field", name: { kind: "Name", value: "totalCount" } },
                { kind: "Field", name: { kind: "Name", value: "activityUrl" } },
                {
                  kind: "Field",
                  name: { kind: "Name", value: "edges" },
                  selectionSet: {
                    kind: "SelectionSet",
                    selections: [
                      { kind: "Field", name: { kind: "Name", value: "id" } },
                      {
                        kind: "FragmentSpread",
                        name: { kind: "Name", value: "CommonCommentEdge" },
                      },
                    ],
                  },
                },
                {
                  kind: "Field",
                  name: { kind: "Name", value: "pageInfo" },
                  selectionSet: {
                    kind: "SelectionSet",
                    selections: [
                      {
                        kind: "Field",
                        name: { kind: "Name", value: "hasPreviousPage" },
                      },
                      {
                        kind: "Field",
                        name: { kind: "Name", value: "hasNextPage" },
                      },
                      {
                        kind: "Field",
                        name: { kind: "Name", value: "startCursor" },
                      },
                      {
                        kind: "Field",
                        name: { kind: "Name", value: "endCursor" },
                      },
                    ],
                  },
                },
              ],
            },
          },
        ],
      },
    },
    {
      kind: "FragmentDefinition",
      name: { kind: "Name", value: "CommonCommentEdge" },
      typeCondition: {
        kind: "NamedType",
        name: { kind: "Name", value: "CommentEdge" },
      },
      selectionSet: {
        kind: "SelectionSet",
        selections: [
          { kind: "Field", name: { kind: "Name", value: "id" } },
          {
            kind: "Field",
            name: { kind: "Name", value: "node" },
            selectionSet: {
              kind: "SelectionSet",
              selections: [
                { kind: "Field", name: { kind: "Name", value: "id" } },
                { kind: "Field", name: { kind: "Name", value: "luid" } },
                { kind: "Field", name: { kind: "Name", value: "parentPath" } },
                { kind: "Field", name: { kind: "Name", value: "childPath" } },
                { kind: "Field", name: { kind: "Name", value: "guid" } },
                { kind: "Field", name: { kind: "Name", value: "body" } },
                { kind: "Field", name: { kind: "Name", value: "timeCreated" } },
                { kind: "Field", name: { kind: "Name", value: "url" } },
                {
                  kind: "Field",
                  name: { kind: "Name", value: "attachment" },
                  selectionSet: {
                    kind: "SelectionSet",
                    selections: [
                      { kind: "Field", name: { kind: "Name", value: "type" } },
                      { kind: "Field", name: { kind: "Name", value: "src" } },
                    ],
                  },
                },
                {
                  kind: "Field",
                  name: { kind: "Name", value: "owner" },
                  selectionSet: {
                    kind: "SelectionSet",
                    selections: [
                      { kind: "Field", name: { kind: "Name", value: "guid" } },
                      { kind: "Field", name: { kind: "Name", value: "name" } },
                      {
                        kind: "Field",
                        name: { kind: "Name", value: "iconUrl" },
                      },
                    ],
                  },
                },
              ],
            },
          },
          { kind: "Field", name: { kind: "Name", value: "repliesCount" } },
          { kind: "Field", name: { kind: "Name", value: "hasVotedUp" } },
          { kind: "Field", name: { kind: "Name", value: "hasVotedDown" } },
          { kind: "Field", name: { kind: "Name", value: "votesUpCount" } },
        ],
      },
    },
  ],
} as unknown as DocumentNode<
  GetEmbeddedCommentsQuery,
  GetEmbeddedCommentsQueryVariables
>;
