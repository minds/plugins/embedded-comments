import Footer from "./app/footer/Footer";
import CommentsThread from "./app/comments/thread/CommentsThread";
import { MutableRefObject, useEffect, useRef } from "react";
import { useSearchParams } from "react-router-dom";
import { useTheme } from "./app/common/theme/UseTheme";

const useMutationObserver = (
  ref: MutableRefObject<any>,
  callback: MutationCallback,
  options = {
    attributes: true,
    characterData: true,
    childList: true,
    subtree: true,
  },
) => {
  useEffect(() => {
    if (ref.current) {
      const observer = new MutationObserver(callback);
      observer.observe(ref.current, options);
      return () => observer.disconnect();
    }
  }, [ref, callback, options]);
};

function App() {
  const mutationRef = useRef(window.document.documentElement);
  const [URLSearchParams] = useSearchParams();

  const updateIframeHeight = () => {
    (window as any).parent.postMessage(
      {
        fn: "AutoMindsEmbeddedCommentsSdkHeight",
        height: document.body.offsetHeight,
      },
      "*",
    );
  };

  useMutationObserver(mutationRef, updateIframeHeight);
  useTheme({
    theme: URLSearchParams.get("theme") ?? "light",
    accentColor: URLSearchParams.get("accentColor") ?? null,
  });

  const branded: boolean =
    URLSearchParams.get("mindsBranded") !== "false" ?? true;

  return (
    <div className="p-4 pt-0">
      <CommentsThread parentPath="0:0:0" />
      {branded && <Footer />}
    </div>
  );
}

export default App;
