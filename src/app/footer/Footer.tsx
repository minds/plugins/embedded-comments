function Footer() {
  return (
    <div>
      <div className="h-10"></div>
      <div className="fixed bottom-0 left-0 right-0 w-full bg-bg-primary text-secondary text-sm">
        <div className="mx-4 border-t border-border-primary py-2">
          <img
            alt="Minds.com"
            src="https://www.minds.com/static/en/assets/logos/bulb.svg"
            className="w-2.5 mr-2 inline align-text-top"
          />
          <a href="https://networks.minds.com/" target="blank noreferrer">
            Powered by Minds Networks
          </a>
        </div>
      </div>
    </div>
  );
}

export default Footer;
