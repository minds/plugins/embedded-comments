import { useQuery } from "@apollo/client";
import { gql } from "../../../__generated__";
import CommentsList from "../list/CommentsList";
import CommentInput from "../input/CommentInput";
import { useSearchParams } from "react-router-dom";
import { ArrowTopRightOnSquareIcon } from "@heroicons/react/20/solid";
import { useConfigStore } from "../../common/config/ConfigContext";
import posthog from "posthog-js";
import { useEffect } from "react";

export const GET_EMBEDDED_COMMENTS = gql(`
  query GetEmbeddedComments(
    $ownerGuid: String!,
    $url: String!,
    $parentPath: String,
    $limit: Int
  ) {
    embeddedComments(
        ownerGuid: $ownerGuid,
        url: $url,
        parentPath: $parentPath
        first: $limit
      ) {
        totalCount
        activityUrl
        edges {
          id
          ...CommonCommentEdge
        }
        pageInfo {
          hasPreviousPage
          hasNextPage
          startCursor
          endCursor
        }
      }
  }
`);

function CommentsThread({ parentPath }: { parentPath: string }) {
  const [URLSearchParams] = useSearchParams();
  const { configs } = useConfigStore();

  useEffect(() => {
    posthog.init(configs.posthog.api_key, {
      api_host: configs.posthog.host,
      capture_pageview: false,
      autocapture: false,
      advanced_disable_feature_flags: true,
    });
  }, [configs]);

  const pageUrl = URLSearchParams.get("pageUrl") || "";
  const ownerGuid = URLSearchParams.get("ownerGuid") || "";

  const { loading, error, data } = useQuery(GET_EMBEDDED_COMMENTS, {
    variables: {
      ownerGuid,
      url: pageUrl,
      parentPath,
      limit: 12,
    },
  });

  useEffect(() => {
    if (data) {
      posthog.capture("embedded_comments_view", {
        tenant_id: configs.tenant_id,
        $set: {
          tenant_id: configs.tenant_id,
        },
      });
    }
  }, [data, configs.tenant_id]);

  if (loading) return <p className="text-secondary">Loading...</p>;

  if (error) return <p className="text-primary">Error : {error.message}</p>;

  if (!data) return <p className="text-primary">Error : no data</p>;

  const edges = data.embeddedComments.edges;

  const isTopLevel = parentPath === "0:0:0";

  const hasMore = data.embeddedComments.pageInfo.hasNextPage;
  const totalCount = data.embeddedComments.totalCount;
  const activityUrl =
    data.embeddedComments.activityUrl + "?ref=embedded-comments";

  return (
    <div>
      {isTopLevel ? (
        <div className="flex items-center py-2 border-b-2 border-border-primary text-primary">
          <span className="font-bold">{totalCount} Comments</span>

          <span className="flex-auto"></span>

          <a href={activityUrl} target="_blank" rel="noopener noreferrer">
            <ArrowTopRightOnSquareIcon className="h-4 w-4 text-secondary inline" />
          </a>
        </div>
      ) : undefined}

      {isTopLevel ? <CommentInput parentPath={parentPath} /> : undefined}

      <CommentsList edges={edges} />

      {hasMore ? (
        <a
          href={activityUrl}
          target="_blank"
          rel="noopener noreferrer"
          className="pt-4 text-accent block"
        >
          <span className="align-middle">View more comments</span>
          <ArrowTopRightOnSquareIcon className="h-4 w-4 ml-1 inline-block align-middle inline" />
        </a>
      ) : undefined}

      {!isTopLevel ? <CommentInput parentPath={parentPath} /> : undefined}
    </div>
  );
}

export default CommentsThread;
