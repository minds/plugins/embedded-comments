import CommentItem from "../item/CommentItem";
import { GetEmbeddedCommentsQuery } from "../../../__generated__/graphql";

interface Props {
  edges: GetEmbeddedCommentsQuery["embeddedComments"]["edges"];
}

function CommentsList({ edges }: Props) {
  return (
    <div className="divide-y divide-border-primary">
      {edges.map((edge) => {
        return <CommentItem edge={edge} key={edge.id} />;
      })}
    </div>
  );
}

export default CommentsList;
