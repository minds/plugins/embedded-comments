import { useCallback, useState } from "react";
import { gql } from "../../../__generated__";
import { useMutation } from "@apollo/client";
import { useSearchParams } from "react-router-dom";
import { GET_EMBEDDED_COMMENTS } from "../thread/CommentsThread";
import { useAuthStore } from "../../common/auth/AuthProvider";
import { useAuthService } from "../../common/auth/AuthService";

const CREATE_COMMENT = gql(`
  mutation CreateComment(
    $ownerGuid: String!,
    $url: String!,
    $parentPath: String!,
    $body: String!
) {
    createEmbeddedComment(
        ownerGuid: $ownerGuid
        url: $url
        parentPath: $parentPath
        body: $body
      ) {
        id
        ...CommonCommentEdge
    }
  }
`);

function CommentInput({ parentPath }: { parentPath: string }) {
  const { user } = useAuthStore();
  const { authetincate } = useAuthService();

  const [body, setBody] = useState("");
  const [showSubmitBtn, setShowSubmitBtn] = useState(false);

  const [URLSearchParams] = useSearchParams();

  const pageUrl = URLSearchParams.get("pageUrl") || "";
  const ownerGuid = URLSearchParams.get("ownerGuid") || "";

  const [createComment, { loading }] = useMutation(CREATE_COMMENT, {
    update: (cache, { data }) => {
      // Get the existing feed
      const cached = cache.readQuery({
        query: GET_EMBEDDED_COMMENTS,
        variables: {
          ownerGuid,
          url: pageUrl,
          parentPath: parentPath,
        },
      });

      const connection = cached?.embeddedComments;

      if (!connection) return;

      if (!data?.createEmbeddedComment) return;

      // Update the cache
      cache.writeQuery({
        query: GET_EMBEDDED_COMMENTS,
        variables: {
          ownerGuid,
          url: pageUrl,
          parentPath,
        },
        data: {
          embeddedComments: {
            ...connection,
            edges: [data.createEmbeddedComment, ...connection.edges],
          },
        },
      });
    },
  });

  const branded: boolean =
    URLSearchParams.get("mindsBranded") !== "false" ?? true;

  const getFallbackAvatarUrl = useCallback((branded: boolean): string => {
    return branded
      ? "https://www.minds.com/static/en/assets/logos/bulb.svg"
      : window.location.pathname + "default-user-avatar.jpg";
  }, []);

  const avatarUrl = user ? user.icon_url : getFallbackAvatarUrl(branded);

  const onFocus = () => {
    setShowSubmitBtn(true);
  };

  /**
   * Submits the comment to the backend. Will check for authentication.
   * @param { boolean } checkLogin - Whether to validate that the user is logged in.
   * @returns { Promise<void> }
   */
  const onSubmit = async (checkLogin: boolean = true): Promise<void> => {
    if (loading) return;
    if (!body.length) return;

    if (!user && checkLogin) {
      await authetincate({
        onSuccess: () => onSubmit(false),
      });
      return;
    }

    try {
      await createComment({
        variables: {
          ownerGuid,
          url: pageUrl,
          parentPath,
          body,
        },
      });
      setBody("");
    } catch (e) {
      console.error("Failed to save comment:", e);
    }
  };

  return (
    <div className={"flex pt-4 pb-6"}>
      <div className={"shrink-0 w-12"}>
        <img
          className="h-12 w-12 object-contain rounded-full"
          src={avatarUrl}
          alt="You"
        />
      </div>
      <div className={"flex-auto pl-4"}>
        <textarea
          className="w-full h-12 border rounded-md p-2 box-border bg-transparent border-border-primary text-primary focus:ring-2 focus:ring-blue outline-none"
          placeholder="Add your comment"
          onFocus={onFocus}
          value={body}
          onChange={(e) => setBody(e.target.value)}
        ></textarea>

        {showSubmitBtn ? (
          <div className="flex">
            <span className="flex-auto"></span>
            <button
              type="submit"
              className="border rounded-md mt-2 py-1 px-4 right-0 text-primary border-border-primary hover:underline"
              onClick={() => onSubmit(true)}
            >
              {loading ? "Posting..." : "Post"}
            </button>
          </div>
        ) : null}
      </div>
    </div>
  );
}

export default CommentInput;
