import moment from "moment";
import { useMemo, useState } from "react";
import CommentsThread from "../thread/CommentsThread";
import { FragmentType, gql, useFragment } from "../../../__generated__";
import CommentMenu from "./CommentMenu";
import { apiFetch } from "../../common/ApiService";
import { useAuthService } from "../../common/auth/AuthService";
import { useAuthStore } from "../../common/auth/AuthProvider";
import { MindsLinkify } from "../../common/linkify/MindsLinkify";

export const COMMENT_EDGE_FRAGMENT = gql(`
fragment CommonCommentEdge on CommentEdge {
  id

  node {
    id
    luid
    parentPath
    childPath
    guid
    body
    timeCreated
    url
    attachment {
      type
      src
    }
    owner {
      guid
      name
      iconUrl
    }
  }
  
  repliesCount
  hasVotedUp
  hasVotedDown
  votesUpCount
}`);

function classNames(...classes: string[]) {
  return classes.filter(Boolean).join(" ");
}

function CommentItem(props: {
  edge: FragmentType<typeof COMMENT_EDGE_FRAGMENT>;
}) {
  const { user } = useAuthStore();
  const { authetincate } = useAuthService();

  const edge = useFragment(COMMENT_EDGE_FRAGMENT, props.edge);

  const [showReplies, setShowReplies] = useState(false);

  const [hasVotedUp, setHasVotedUp] = useState(edge.hasVotedUp);
  const [votesUpCount, setVotesUpCount] = useState(edge.votesUpCount);

  const node = edge.node;

  const avatarUrl = node.owner.iconUrl;

  const momentTs = moment.unix(node.timeCreated);
  const friendlyTime = momentTs.fromNow();
  const fullTime = momentTs.format();

  const onReplyClick = () => {
    setShowReplies(!showReplies);
  };

  /**
   * Submits the bote up to the backend. Will check for authentication.
   * @param { boolean } checkLogin - Whether to validate that the user is logged in.
   * @returns { Promise<void> }
   */
  const onVoteUpClick = async (checkLogin: boolean = true): Promise<void> => {
    if (!user && checkLogin) {
      await authetincate({
        onSuccess: () => onVoteUpClick(false),
      });
      return;
    }

    setVotesUpCount(votesUpCount + (hasVotedUp ? -1 : 1));
    setHasVotedUp(!hasVotedUp);

    try {
      const res: Response = await apiFetch(
        "/api/v1/thumbs/" + node.luid + "/up",
        {
          method: hasVotedUp ? "DELETE" : "PUT",
        },
      );

      if (res.status !== 200) {
        throw new Error(res?.statusText);
      }
    } catch (e) {
      console.log("Failed to save vote up:", e);
      // Revert to original values.
      setVotesUpCount(votesUpCount);
      setHasVotedUp(hasVotedUp);
    }
  };

  // whether reply option should be shown.
  const shouldShowReplyOption: boolean = useMemo(() => {
    return (
      node.childPath
        ?.split(":")
        ?.filter((parentCommentId: string): boolean => parentCommentId === "0")
        ?.length >= 1
    );
  }, [node]);

  return (
    <div className={"flex py-4 first:pt-0 last:pb-0"}>
      <div className={"shrink-0 w-12"}>
        <img
          className="h-12 w-12 object-cover rounded-full"
          src={avatarUrl}
          alt={node.owner.name}
        />
      </div>
      <div className={"flex-auto pl-4 text-primary"}>
        <strong className="text-primary">{node.owner.name}</strong>

        <p className="pb-2">
          <MindsLinkify>{node.body}</MindsLinkify>
          {/* Video attachments are currently only supported as links. */}
          {Boolean(node?.url && node.attachment?.type === "video") && (
            <>
              {node.body?.length > 0 && <span>&nbsp;</span>}
              <a
                className="text-accent visited:text-purple-600"
                href={node.url}
                target="_blank"
                rel="noreferrer"
              >
                [Video]
              </a>
            </>
          )}
        </p>

        {Boolean(node.attachment?.src) && node.attachment?.type === "image" && (
          <a
            className="block w-fit"
            href={node.url}
            target="_blank"
            rel="noreferrer"
          >
            <img
              className="max-h-64 min-h-64 max-w-full w-auto pb-2"
              src={node.attachment?.src}
              alt="Comment attachment"
            ></img>
          </a>
        )}

        <div className="flex text-sm text-secondary space-x-3 items-center">
          <div
            className={hasVotedUp ? "text-accent" : undefined}
            onClick={() => onVoteUpClick(true)}
          >
            <svg
              xmlns="http://www.w3.org/2000/svg"
              fill="none"
              viewBox="0 0 24 24"
              strokeWidth={1.5}
              stroke="currentColor"
              className={classNames(
                hasVotedUp ? "fill-accent" : "",
                "cursor-pointer w-4 h-4 transform -scale-x-100 inline",
              )}
            >
              <path
                strokeLinecap="round"
                strokeLinejoin="round"
                d="M6.633 10.5c.806 0 1.533-.446 2.031-1.08a9.041 9.041 0 012.861-2.4c.723-.384 1.35-.956 1.653-1.715a4.498 4.498 0 00.322-1.672V3a.75.75 0 01.75-.75A2.25 2.25 0 0116.5 4.5c0 1.152-.26 2.243-.723 3.218-.266.558.107 1.282.725 1.282h3.126c1.026 0 1.945.694 2.054 1.715.045.422.068.85.068 1.285a11.95 11.95 0 01-2.649 7.521c-.388.482-.987.729-1.605.729H13.48c-.483 0-.964-.078-1.423-.23l-3.114-1.04a4.501 4.501 0 00-1.423-.23H5.904M14.25 9h2.25M5.904 18.75c.083.205.173.405.27.602.197.4-.078.898-.523.898h-.908c-.889 0-1.713-.518-1.972-1.368a12 12 0 01-.521-3.507c0-1.553.295-3.036.831-4.398C3.387 10.203 4.167 9.75 5 9.75h1.053c.472 0 .745.556.5.96a8.958 8.958 0 00-1.302 4.665c0 1.194.232 2.333.654 3.375z"
              />
            </svg>

            <span className="ml-2 align-middle">
              {votesUpCount ? `(${votesUpCount})` : undefined}
            </span>
          </div>

          {shouldShowReplyOption && (
            <span
              className="cursor-pointer text-secondary"
              onClick={onReplyClick}
            >
              Reply
            </span>
          )}

          <span title={fullTime}>{friendlyTime}</span>

          <span className="flex-auto"></span>

          <CommentMenu edge={edge} />
        </div>

        {edge.repliesCount > 0 ? (
          <span
            className="cursor-pointer text-sm text-accent"
            onClick={onReplyClick}
          >
            View {edge.repliesCount} replies
          </span>
        ) : null}

        {showReplies ? (
          <div className="mt-4">
            <CommentsThread parentPath={node.childPath} />
          </div>
        ) : null}
      </div>
    </div>
  );
}

export default CommentItem;
