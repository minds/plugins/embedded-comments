import { Fragment } from "react";
import { Menu, Transition } from "@headlessui/react";
import {
  ChevronDownIcon,
  ArrowTopRightOnSquareIcon,
} from "@heroicons/react/20/solid";
import { useAuthStore } from "../../common/auth/AuthProvider";
import { CommonCommentEdgeFragment } from "../../../__generated__/graphql";
import { apiFetch } from "../../common/ApiService";

function CommentMenu(props: { edge: CommonCommentEdgeFragment }) {
  const { user } = useAuthStore();

  const edge = props.edge;

  const isOwner = user?.guid === edge.node.owner.guid;
  const commentUrl = edge.node.url;

  // const onReport = () => {

  // };

  const onDelete = async () => {
    if (!window.confirm("Are you sure?")) return;

    await apiFetch("/api/v1/comments/" + edge.node.luid, { method: "DELETE" });

    window.location.reload();
  };

  return (
    <Menu as="div" className="relative inline-block text-left">
      <div>
        <Menu.Button className="inline-flex w-full justify-center px-4 hover:opacity-50">
          <ChevronDownIcon
            className="h-5 w-5 text-secondary"
            aria-hidden="true"
          />
        </Menu.Button>

        <Transition
          as={Fragment}
          enter="transition ease-out duration-100"
          enterFrom="transform opacity-0 scale-95"
          enterTo="transform opacity-100 scale-100"
          leave="transition ease-in duration-75"
          leaveFrom="transform opacity-100 scale-100"
          leaveTo="transform opacity-0 scale-95"
        >
          <Menu.Items className="absolute right-0 z-10 mt-2 w-56 origin-top-right divide-y divide-border-primary rounded-md bg-bg-secondary shadow-lg ring-1 ring-black ring-opacity-5 focus:outline-none">
            <div className="py-1">
              <Menu.Item>
                {({ active }) => (
                  <a
                    href={commentUrl}
                    target="_blank"
                    rel="noreferrer"
                    className={
                      "block px-4 py-2 text-sm text-primary flex items-center hover:underline"
                    }
                  >
                    <span className="align-middle flex-1"> Go to link </span>
                    <ArrowTopRightOnSquareIcon className="ml-1 h-4 w-4 text-secondary inline " />
                  </a>
                )}
              </Menu.Item>
            </div>

            {/* <div className="py-1">
                            <Menu.Item>
                                {({ active }) => (
                                    <span
                                        onClick={onReport}
                                        className={classNames(
                                            active ? 'bg-gray-100 text-gray-900' : 'text-gray-700',
                                            'block px-4 py-2 text-sm cursor-pointer'
                                        )}
                                    >
                                        Report
                                    </span>
                                )}
                            </Menu.Item>
                        </div> */}

            {isOwner ? (
              <div className="py-1">
                <Menu.Item>
                  {({ active }) => (
                    <span
                      onClick={onDelete}
                      className={
                        "block px-4 py-2 text-sm text-red-500 cursor-pointer hover:underline"
                      }
                    >
                      Delete
                    </span>
                  )}
                </Menu.Item>
              </div>
            ) : undefined}
          </Menu.Items>
        </Transition>
      </div>
    </Menu>
  );
}

export default CommentMenu;
