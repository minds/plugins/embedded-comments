import { useConfigStore } from "../config/ConfigContext";
import { FC, ReactElement, createContext, useEffect } from "react";

import { createStore, useStore } from "zustand";

interface AuthState {
  user: any;
  setUser: (user: any) => void;
}

export const AuthStore = createStore<AuthState>()((set) => ({
  user: null,
  setUser: (user) => {
    set({ user });
  },
}));

export const AuthContext = createContext<typeof AuthStore>(AuthStore);

type ProviderProps = {
  children: ReactElement;
};

export const AuthProvider: FC<ProviderProps> = (props) => {
  const { configs } = useConfigStore();
  const { setUser } = useAuthStore();

  useEffect(() => {
    setUser(configs.user);
  }, [setUser, configs.user]);

  return (
    <AuthContext.Provider value={AuthStore}>
      {props.children}
    </AuthContext.Provider>
  );
};

export const useAuthStore = () => useStore(AuthStore);
