import { useConfigStore } from "../config/ConfigContext";

type AuthService = {
  authetincate: (params: AuthenticateParameters) => Promise<void>;
};

type AuthenticateParameters = { onSuccess?: Function; onFailure?: Function };

/**
 * Custom hook for auth service.
 * @returns { AuthService } - auth service.
 */
export const useAuthService = (): AuthService => {
  const { fetchConfigs, getConfigs } = useConfigStore();

  /**
   * Authenticate a user by opening a new window to request login. When the window closes
   * we will reload configs and call the appropriate callback function.
   * @param { AuthenticateParameters } params - onSuccess and onFailure callbacks.
   * @returns { Promise<void> }
   */
  const authetincate = async ({
    onSuccess,
    onFailure,
  }: AuthenticateParameters): Promise<void> => {
    // Safari requires this
    try {
      await document.requestStorageAccess();
    } catch (err) {
      console.warn(err);
    }

    const redirectUrl = `https://${window.location.host}/api/v3/embedded-comments/auth/success`;
    const windowRef = window.open(
      "/register?redirectUrl=" + encodeURI(redirectUrl),
      "minds-login",
    );

    if (windowRef) {
      const timer = setInterval(async () => {
        if (windowRef.closed && document.hasFocus()) {
          // Stop polling
          clearInterval(timer);

          // Refetch the configs and grab the user.
          await fetchConfigs();

          // Run appropriate callback function if present.
          Boolean(getConfigs()?.user) ? onSuccess?.() : onFailure?.();
        }
      }, 500);
    }
    return;
  };

  return { authetincate };
};
