export const getXsrfCookie = () => {
  return document.cookie.replace(
    /(?:(?:^|.*;\s*)XSRF-TOKEN\s*=\s*([^;]*).*$)|^.*$/,
    "$1",
  );
};

export const apiFetch = async (
  input: RequestInfo | URL,
  init?: RequestInit | undefined,
) => {
  const xsrfCookieValue = getXsrfCookie();

  return fetch(input, {
    ...init,
    headers: {
      "X-XSRF-TOKEN": xsrfCookieValue,
    },
  });
};
