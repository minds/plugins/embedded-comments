import { FC, ReactElement, createContext, useEffect } from "react";
import { createStore, useStore } from "zustand";

interface ConfigState {
  isReady: boolean;
  configs: any;
  fetchConfigs: () => Promise<void>;
  getConfigs: () => any;
}

export const ConfigStore = createStore<ConfigState>()((set, get) => ({
  isReady: false,
  configs: null,
  fetchConfigs: async () => {
    const response = await fetch("/api/v1/minds/config", {
      credentials: "include",
    });
    set({
      isReady: true,
      configs: await response.json(),
    });
  },
  getConfigs: (): any => {
    return get().configs;
  },
}));

const ConfigContext = createContext<typeof ConfigStore>(ConfigStore);

type ProviderProps = {
  children: ReactElement;
};

export const ConfigProvider: FC<ProviderProps> = (props) => {
  const { isReady, fetchConfigs } = useStore(ConfigStore);

  useEffect(() => {
    fetchConfigs();
  }, [fetchConfigs]);

  return (
    <ConfigContext.Provider value={ConfigStore}>
      {isReady ? props.children : undefined}
    </ConfigContext.Provider>
  );
};

export const useConfigStore = () => useStore(ConfigStore);
