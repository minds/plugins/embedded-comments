import Linkify from "linkify-react";
import "linkify-plugin-hashtag";
import "linkify-plugin-mention";
import { PropsWithChildren } from "react";

/**
 * Linkify component. Will convert URLs, hashtags and mentions to links.
 * @param { PropsWithChildren } props - simple children prop.
 * @returns { JSX.Element } - MindsLinkify component.
 */
export const MindsLinkify = ({ children }: PropsWithChildren) => {
  const options: Object = {
    formatHref: {
      hashtag: (href: string) =>
        window.location.origin +
        "/discovery/search?f=top&t=all&q=%23" +
        href.substring(1),
      mention: (href: string) => window.location.origin + href,
    },
    attributes: {
      onClick: (event: any) => event.stopPropagation(), // Prevent outward propagation.
    },
    target: "_blank",
    className: "text-accent hover:underline",
    nl2br: true, // preserve \n line breaks.
  };
  return <Linkify options={options}>{children}</Linkify>;
};
