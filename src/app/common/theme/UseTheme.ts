import { useEffect } from "react";

/** Theming options. */
export type ThemeOptions = {
  theme?: string | null;
  accentColor?: string | null;
};

/**
 * Handles theme changes by applying theme classes, and setting properties in the DOM.
 * @param { ThemeOptions } opts - Theme options.
 * @returns { void }
 */
export const useTheme = (
  { theme, accentColor }: ThemeOptions = { theme: "light" },
): void => {
  useEffect(() => {
    if (theme) {
      document.documentElement.classList.remove("dark", "light");
      document.documentElement.classList.add(theme);
    }
    if (accentColor) {
      document.documentElement.style.setProperty("--color-accent", accentColor);
    }
  }, [theme, accentColor]);
};
