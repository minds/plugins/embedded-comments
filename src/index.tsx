import React from "react";
import ReactDOM from "react-dom/client";
import { createBrowserRouter, RouterProvider } from "react-router-dom";
import {
  ApolloClient,
  InMemoryCache,
  ApolloProvider,
  HttpLink,
  ApolloLink,
} from "@apollo/client";
import "./index.css";
import App from "./App";
import reportWebVitals from "./reportWebVitals";
import { relayStylePagination } from "@apollo/client/utilities";
import { getXsrfCookie } from "./app/common/ApiService";
import { ConfigProvider } from "./app/common/config/ConfigContext";
import { AuthProvider } from "./app/common/auth/AuthProvider";

/**
 * Apollo setup
 */
const httpLink = new HttpLink({
  uri: "/api/graphql",
});

const authLink = new ApolloLink((operation, forward) => {
  const xsrfCookieValue = getXsrfCookie();

  operation.setContext({
    headers: {
      "X-XSRF-TOKEN": xsrfCookieValue,
    },
  });

  return forward(operation);
});

const client = new ApolloClient({
  link: authLink.concat(httpLink),
  cache: new InMemoryCache({
    typePolicies: {
      Query: {
        fields: {
          embeddedComments: relayStylePagination(["url", "parentPath"]),
        },
      },
    },
  }),
});

/**
 * Router setup
 */
const router = createBrowserRouter(
  [
    {
      path: "/",
      element: <App />,
    },
  ],
  {
    basename: "/plugins/embedded-comments",
  },
);

/**
 * React setup
 */
const root = ReactDOM.createRoot(
  document.getElementById("root") as HTMLElement,
);

root.render(
  <React.StrictMode>
    <ConfigProvider>
      <AuthProvider>
        <ApolloProvider client={client}>
          <RouterProvider router={router} />
        </ApolloProvider>
      </AuthProvider>
    </ConfigProvider>
  </React.StrictMode>,
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
