/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./src/**/*.{js,jsx,ts,tsx}"],
  darkMode: 'class',
  theme: {
    extend: {
      scale: {
        "-100": "-1",
      },
      colors: {
        current: "currentColor",
        transparent: "transparent",
        primary: 'var(--color-text-primary)',
        secondary: 'var(--color-text-secondary)',
        border: {
          primary: 'var(--color-border-primary)'
        },
        bg: {
          primary: 'var(--color-bg-primary)',
          secondary: 'var(--color-bg-secondary)',
        },
        accent: 'var(--color-accent)'
      },
    },
  },
  plugins: [],
};
